# translation of docs_krita_org_reference_manual___tools___elliptical_select.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___tools___elliptical_select\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-04-02 13:13+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "Ratio"
msgstr "Pomer"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
#, fuzzy
#| msgid ""
#| ".. image:: images/icons/Krita_mouse_left.png\n"
#| "   :alt: mouseleft"
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:68
msgid ""
".. image:: images/icons/elliptical_select_tool.svg\n"
"   :alt: toolselectellipse"
msgstr ""
".. image:: images/icons/elliptical_select_tool.svg\n"
"   :alt: toolselectellipse"

#: ../../reference_manual/tools/elliptical_select.rst:1
msgid "Krita's elliptical selector tool reference."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:11
msgid "Ellipse"
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:11
msgid "Circle"
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:11
#, fuzzy
#| msgid "Elliptical Selection Tool"
msgid "Elliptical Select"
msgstr "Nástroj eliptický výber"

#: ../../reference_manual/tools/elliptical_select.rst:11
msgid "Selection"
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:16
msgid "Elliptical Selection Tool"
msgstr "Nástroj eliptický výber"

#: ../../reference_manual/tools/elliptical_select.rst:18
msgid "|toolselectellipse|"
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:20
msgid ""
"This tool, represented by an ellipse with a dashed border, allows you to "
"make :ref:`selections_basics` of a elliptical area. Simply click and drag "
"around the section you wish to select."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:23
msgid "Hotkeys and Stickykeys"
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:25
msgid ":kbd:`J` selects this tool."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:26
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:27
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:28
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:29
msgid ""
":kbd:`Shift` after starting the selection, constraints it to a perfect "
"circle."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:30
msgid ""
":kbd:`Ctrl` after starting the selection, makes the selection resize from "
"center."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:31
msgid ":kbd:`Alt` after starting the selection, allows you to move it."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:32
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:33
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:34
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:35
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to  "
"'intersect'."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:39
msgid "Hovering over a selection allows you to move it."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:40
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:44
msgid ""
"So to subtract a perfect circle, you do :kbd:`Alt +` |mouseleft|, then "
"release the :kbd:`Alt` key while dragging and press the :kbd:`Shift` key to "
"constrain."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:49
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:52
msgid "Tool Options"
msgstr "Voľby nástroja"

#: ../../reference_manual/tools/elliptical_select.rst:54
msgid "Anti-aliasing"
msgstr "Vyhladzovanie"

#: ../../reference_manual/tools/elliptical_select.rst:55
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:56
msgid "Width"
msgstr "Šírka"

#: ../../reference_manual/tools/elliptical_select.rst:57
msgid ""
"Gives the current width. Use the lock to force the next selection made to "
"this width."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:58
msgid "Height"
msgstr "Výška"

#: ../../reference_manual/tools/elliptical_select.rst:59
msgid ""
"Gives the current height. Use the lock to force the next selection made to "
"this height."
msgstr ""

#: ../../reference_manual/tools/elliptical_select.rst:61
msgid ""
"Gives the current ratio. Use the lock to force the next selection made to "
"this ratio."
msgstr ""
