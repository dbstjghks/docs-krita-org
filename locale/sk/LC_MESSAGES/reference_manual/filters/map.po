# translation of docs_krita_org_reference_manual___filters___map.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters___map\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 10:30+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/filters/map.rst:1
msgid "Overview of the map filters."
msgstr ""

#: ../../reference_manual/filters/map.rst:11
msgid "Filters"
msgstr ""

#: ../../reference_manual/filters/map.rst:16
msgid "Map"
msgstr "Mapa"

#: ../../reference_manual/filters/map.rst:18
msgid "Filters that are signified by them mapping the input image."
msgstr ""

#: ../../reference_manual/filters/map.rst:20
#: ../../reference_manual/filters/map.rst:23
msgid "Small Tiles"
msgstr "Malé dlaždice"

#: ../../reference_manual/filters/map.rst:20
#, fuzzy
#| msgid "Small Tiles"
msgid "Tiles"
msgstr "Malé dlaždice"

#: ../../reference_manual/filters/map.rst:25
msgid "Tiles the input image, using its own layer as output."
msgstr ""

#: ../../reference_manual/filters/map.rst:27
msgid "Height Map"
msgstr ""

#: ../../reference_manual/filters/map.rst:27
#, fuzzy
#| msgid "Phong Bumpmap"
msgid "Bumpmap"
msgstr "PhongBumpmap"

#: ../../reference_manual/filters/map.rst:27
#, fuzzy
#| msgid "Normalize"
msgid "Normal Map"
msgstr "Normalizovať"

#: ../../reference_manual/filters/map.rst:30
msgid "Phong Bumpmap"
msgstr "PhongBumpmap"

#: ../../reference_manual/filters/map.rst:33
#, fuzzy
#| msgid ".. image:: images/en/Krita-normals-tutoria_4.png"
msgid ".. image:: images/brushes/Krita-normals-tutoria_4.png"
msgstr ".. image:: images/en/Krita-normals-tutoria_4.png"

#: ../../reference_manual/filters/map.rst:34
msgid ""
"Uses the input image as a height-map to output a 3d something, using the "
"phong-lambert shading model. Useful for checking one's height maps during "
"game texturing. Checking the :guilabel:`Normal Map` box will make it use all "
"channels and interpret them as a normal map."
msgstr ""

#: ../../reference_manual/filters/map.rst:37
msgid "Round Corners"
msgstr "Zaoblené rohy"

#: ../../reference_manual/filters/map.rst:39
msgid "Adds little corners to the input image."
msgstr ""

#: ../../reference_manual/filters/map.rst:41
#: ../../reference_manual/filters/map.rst:44
msgid "Normalize"
msgstr "Normalizovať"

#: ../../reference_manual/filters/map.rst:46
msgid ""
"This filter takes the input pixels, puts them into a 3d vector, and then "
"normalizes (makes the vector size exactly 1) the values. This is helpful for "
"normal maps and some minor image-editing functions."
msgstr ""

#: ../../reference_manual/filters/map.rst:48
#: ../../reference_manual/filters/map.rst:51
msgid "Gradient Map"
msgstr "Mapa prechodov"

#: ../../reference_manual/filters/map.rst:48
#, fuzzy
#| msgid "Gradient Map"
msgid "Gradient"
msgstr "Mapa prechodov"

#: ../../reference_manual/filters/map.rst:54
#, fuzzy
#| msgid ".. image:: images/en/Krita_filter_gradient_map.png"
msgid ".. image:: images/filters/Krita_filter_gradient_map.png"
msgstr ".. image:: images/en/Krita_filter_gradient_map.png"

#: ../../reference_manual/filters/map.rst:55
msgid ""
"Maps the lightness of the input to the selected gradient. Useful for fancy "
"artistic effects."
msgstr ""

#: ../../reference_manual/filters/map.rst:57
msgid ""
"In 3.x you could only select predefined gradients. In 4.0, you can select "
"gradients and change them on the fly, as well as use the gradient map filter "
"as a filter layer or filter brush."
msgstr ""
