# translation of docs_krita_org_reference_manual___main_menu___image_menu.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___main_menu___image_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-04-02 12:11+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "Separate Image"
msgstr "Oddeliť obrázok"

#: ../../reference_manual/main_menu/image_menu.rst:1
msgid "The image menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
#, fuzzy
#| msgid "Shear Image"
msgid "Image"
msgstr "Orezať obrázok"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Canvas Projection Color"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Trim"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
#, fuzzy
#| msgid "Resize Canvas"
msgid "Resize"
msgstr "Zmeniť veľkosť plátna"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Scale"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Mirror"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Transform"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Convert Color Space"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:11
#, fuzzy
#| msgid "Offset Image"
msgid "Offset"
msgstr "Posun obrázku"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Split Channels"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:16
msgid "Image Menu"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:18
msgid "Properties"
msgstr "Vlastnosti"

#: ../../reference_manual/main_menu/image_menu.rst:19
msgid "Gives you the image properties."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:20
msgid "Image Background Color and Transparency"
msgstr "Farba pozadia obrázku a priehľadnosť"

#: ../../reference_manual/main_menu/image_menu.rst:21
msgid "Change the background canvas color."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:22
msgid "Convert Current Image Color Space."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:23
msgid "Converts the current image to a new colorspace."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:24
msgid "Trim to image size"
msgstr "Orezať na veľkosť obrázka"

#: ../../reference_manual/main_menu/image_menu.rst:25
msgid ""
"Trims all layers to the image size. Useful for reducing filesize at the loss "
"of information."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:26
msgid "Trim to Current Layer"
msgstr "Orezať na aktuálnu vrstvu"

#: ../../reference_manual/main_menu/image_menu.rst:27
msgid ""
"A lazy cropping function. Krita will use the size of the current layer to "
"determine where to crop."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:28
msgid "Trim to Selection"
msgstr "Orezať na výber"

#: ../../reference_manual/main_menu/image_menu.rst:29
msgid ""
"A lazy cropping function. Krita will crop the canvas to the selected area."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:30
msgid "Rotate Image"
msgstr "Otočiť obrázok"

#: ../../reference_manual/main_menu/image_menu.rst:31
msgid "Rotate the image"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:32
msgid "Shear Image"
msgstr "Orezať obrázok"

#: ../../reference_manual/main_menu/image_menu.rst:33
msgid "Shear the image"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:34
msgid "Mirror Image Horizontally"
msgstr "Zrkadliť obrázok horizontálne"

#: ../../reference_manual/main_menu/image_menu.rst:35
msgid "Mirror the image on the horizontal axis."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:36
msgid "Mirror Image Vertically"
msgstr "Zrkadliť obrázok vertikálne"

#: ../../reference_manual/main_menu/image_menu.rst:37
msgid "Mirror the image on the vertical axis."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:38
msgid "Scale to New Size"
msgstr "Mierka na novú veľkosť"

#: ../../reference_manual/main_menu/image_menu.rst:39
msgid ""
"The resize function in any other program with the :kbd:`Ctrl + Alt + I` "
"shortcut."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:40
msgid "Offset Image"
msgstr "Posun obrázku"

#: ../../reference_manual/main_menu/image_menu.rst:41
msgid "Offset all layers."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:42
msgid "Resize Canvas"
msgstr "Zmeniť veľkosť plátna"

#: ../../reference_manual/main_menu/image_menu.rst:43
msgid "Change the canvas size. Don't confuse this with Scale to new size."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:44
msgid "Image Split"
msgstr "Rozdelenie obrázku"

#: ../../reference_manual/main_menu/image_menu.rst:45
msgid "Calls up the :ref:`image_split` dialog."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:46
msgid "Wavelet Decompose"
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:47
msgid "Does :ref:`wavelet_decompose` on the current layer."
msgstr ""

#: ../../reference_manual/main_menu/image_menu.rst:49
msgid ":ref:`Separates <separate_image>` the image into channels."
msgstr ""
