msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 14:05+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Krita\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../tutorials/krita-brush-tips.rst:1
#: ../../tutorials/krita-brush-tips.rst:13
msgid ""
"Krita Brush-tips is an archive of brush-modification tutorials done by the "
"krita-foundation.tumblr.com account based on user requests."
msgstr ""
"As Pontas de Pincéis do Krita são um arquivo com tutoriais de modificação "
"dos pincéis feitos pela conta 'krita-foundation.tumblr.com' com base nos "
"pedidos dos utilizadores."

#: ../../tutorials/krita-brush-tips.rst:15
msgid "Topics:"
msgstr "Tópicos:"
