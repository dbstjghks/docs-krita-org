# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:05+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en guilabel image Kritamouseleft menuselection\n"
"X-POFile-SpellExtra: Kritamousemiddle kbd Tabletes images\n"
"X-POFile-SpellExtra: KritaOpacitySlider alt popuppalette icons ref kit\n"
"X-POFile-SpellExtra: mouseright palettedocker KritaToolbar Krita\n"
"X-POFile-SpellExtra: Kritamouseright tour mousemiddle brushpresetdocker\n"
"X-POFile-SpellExtra: mouseleft button chooser workspace\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: botão do meio"

#: ../../user_manual/getting_started/navigation.rst:None
msgid ".. image:: images/Interface-tour.svg"
msgstr ".. image:: images/Interface-tour.svg"

#: ../../user_manual/getting_started/navigation.rst:None
msgid ".. image:: images/Krita-popuppalette.png"
msgstr ".. image:: images/Krita-popuppalette.png"

#: ../../user_manual/getting_started/navigation.rst:1
msgid "Overview of Krita navigation."
msgstr "Introdução à navegação do Krita."

#: ../../user_manual/getting_started/navigation.rst:13
#: ../../user_manual/getting_started/navigation.rst:18
#: ../../user_manual/getting_started/navigation.rst:43
msgid "Navigation"
msgstr "Navegação"

#: ../../user_manual/getting_started/navigation.rst:13
#: ../../user_manual/getting_started/navigation.rst:103
msgid "Pop-up Palette"
msgstr "Paleta"

#: ../../user_manual/getting_started/navigation.rst:13
msgid "Zoom"
msgstr "Ampliação"

#: ../../user_manual/getting_started/navigation.rst:13
msgid "Rotate"
msgstr "Rodar"

#: ../../user_manual/getting_started/navigation.rst:13
msgid "Pan"
msgstr "Deslocamento"

#: ../../user_manual/getting_started/navigation.rst:13
msgid "Workspace"
msgstr "Área de Trabalho"

#: ../../user_manual/getting_started/navigation.rst:21
msgid "Interface"
msgstr "Interface"

#: ../../user_manual/getting_started/navigation.rst:23
msgid ""
"Krita's interface is very flexible and provides an ample choice for the "
"artists to arrange the elements of the workspace. An artist can snap and "
"arrange the elements, much like snapping together Lego blocks. Krita "
"provides a set of construction kit parts in the form of Dockers and "
"Toolbars. Every set of elements can be shown, hidden, moved and rearranged "
"that let the artists easily customize their own user interface experience."
msgstr ""
"O Krita é bastante flexível e oferece uma grande variedade de opções para os "
"artistas organizarem o espaço de trabalho. Poderá ajustar e organizar os "
"elementos da área de trabalho, assim como poderá encaixar as peças de Lego. "
"O Krita oferece um conjunto de peças de construção sob a forma de Áreas "
"Acopláveis e Barras de Ferramentas. Todos esses conjuntos de elementos "
"poderão ser apresentados, escondidos, movidos e reorganizados de forma a que "
"o artista possa personalizar facilmente a sua própria experiência com a "
"interface do utilizador."

#: ../../user_manual/getting_started/navigation.rst:26
msgid "A Tour of the Krita Interface"
msgstr "Uma Viagem pela Interface do Krita"

#: ../../user_manual/getting_started/navigation.rst:28
msgid ""
"As we've said before, the Krita interface is very malleable and the way that "
"you choose to configure the work surface may not resemble those shown below, "
"but we can use these as a starting point."
msgstr ""
"Como dissemos anteriormente, a interface do Krita é bastante maleável e a "
"forma como opta por configurar a área de trabalho poderá não ser semelhante "
"á das outras ferramentas mas podemos usar estas definições como um ponto de "
"partida."

#: ../../user_manual/getting_started/navigation.rst:34
msgid ""
"**A** -- Traditional :guilabel:`File` or action menu found in most windowed "
"applications."
msgstr ""
"**A** -- Menu tradicional de :guilabel:`Ficheiro` ou de acções, o qual "
"existe na maioria das aplicações com janelas."

#: ../../user_manual/getting_started/navigation.rst:35
msgid ""
"**B** -- Toolbar - This is where you can choose your brushes, set parameters "
"such as opacity and size and other settings."
msgstr ""
"**B** -- Barra de Ferramentas - Aqui é onde poderá escolher os seus pincéis, "
"definir parâmetros como a opacidade e o tamanho, bem como outras opções."

#: ../../user_manual/getting_started/navigation.rst:36
msgid ""
"**C** -- Sidebars for the Movable Panels/Dockers. In some applications, "
"these are known as Dockable areas. Krita also allows you to dock panels at "
"the top and/or bottom as well."
msgstr ""
"**C** -- Barras Laterais para as Áreas/Painéis Acopláveis. Em algumas das "
"aplicações, são chamadas de Áreas Acopláveis. O Krita também lhe permite "
"acoplar os painéis nas partes superior ou inferior."

#: ../../user_manual/getting_started/navigation.rst:37
msgid ""
"**D** -- Status Bar - This space shows the preferred mode for showing "
"selection i.e. marching ants or mask mode, your selected brush preset, :ref:"
"`Color Space <color_managed_workflow>`, image size and provides a convenient "
"zoom control."
msgstr ""
"**D** -- Barra de Estado - Este espaço mostra-lhe o modo preferido para "
"mostrar a selecção, i.e. o modo de máscara ou das formigas em andamento, a "
"sua predefinição de pincel seleccionada, o :ref:`Espaço de Cores "
"<color_managed_workflow>`, o tamanho da imagem e oferece um controlo de "
"ampliação conveniente."

#: ../../user_manual/getting_started/navigation.rst:38
msgid ""
"**E** -- Floating Panel/Docker - These can be \"popped\" in and out of their "
"docks at any time in order to see a greater range of options. A good example "
"of this would be the :ref:`brush_preset_docker` or the :ref:`palette_docker`."
msgstr ""
"**E** -- Área/Painel Flutuante - Estas poderão ser \"separadas\" ou "
"\"anexadas\" em qualquer altura, para poder ver um maior grupo de opções. Um "
"bom exemplo disso seria a :ref:`brush_preset_docker` ou a :ref:"
"`palette_docker`."

#: ../../user_manual/getting_started/navigation.rst:40
msgid ""
"Your canvas sits in the middle and unlike traditional paper or even most "
"digital painting applications, Krita provides the artist with a scrolling "
"canvas of infinite size (not that you'll need it of course!). The standard "
"navigation tools are as follows:"
msgstr ""
"A sua área de desenho situa-se a meio e, ao contrário do papel tradicional "
"ou mesmo de outras aplicações de pintura, o Krita oferece ao artista uma "
"área de desenho deslizante com tamanho infinito (não que vá necessitar dela, "
"como é óbvio!). As ferramentas de navegação normais são as seguintes:"

#: ../../user_manual/getting_started/navigation.rst:44
msgid ""
"Many of the canvas navigation actions, like rotation, mirroring and zooming "
"have default keys attached to them:"
msgstr ""
"Muitas das acções de navegação na área de desenho, como a rotação, o espelho "
"e a ampliação, têm teclas predefinidas associadas a elas:"

#: ../../user_manual/getting_started/navigation.rst:46
msgid "Panning"
msgstr "Posicionamento"

#: ../../user_manual/getting_started/navigation.rst:47
msgid ""
"This can be done through |mousemiddle|, :kbd:`Space +` |mouseleft| and :kbd:"
"`the directional keys`."
msgstr ""
"Isto poderá ser feito através do |mousemiddle|, do :kbd:`Espaço +` |"
"mouseleft| e as :kbd:`teclas de cursores`."

#: ../../user_manual/getting_started/navigation.rst:48
msgid "Zooming"
msgstr "Ampliação"

#: ../../user_manual/getting_started/navigation.rst:49
msgid ""
"Discrete zooming can be done through :kbd:`+` and :kbd:`-` keys. Using the :"
"kbd:`Ctrl + Space` or :kbd:`Ctrl +` |mousemiddle| shortcuts can allow for "
"direct zooming with the stylus."
msgstr ""
"Poderá fazer alguma ampliação em passos discretos através do :kbd:`+` e do :"
"kbd:`-`. Se usar o :kbd:`Ctrl + Espaço` ou :kbd:`Ctrl +` |mousemiddle|, "
"poderá ampliar directamente com o lápis."

#: ../../user_manual/getting_started/navigation.rst:50
msgid "Mirroring"
msgstr "Espelho"

#: ../../user_manual/getting_started/navigation.rst:51
msgid ""
"You can mirror the view can be quickly done via :kbd:`M` key. Mirroring is a "
"great technique that seasoned digital artists use to quickly review the "
"composition of their work to ensure that it \"reads\" well, even when "
"flipped horizontally."
msgstr ""
"Poderá criar um espelho da janela; isso poderá ser feito rapidamente com o :"
"kbd:`M`. O Espelho é uma técnica óptima que os artistas digitais usam para "
"rever rapidamente a composição do seu trabalho, para garantir que fica "
"\"legível\", mesmo quando estiver invertido na horizontal."

#: ../../user_manual/getting_started/navigation.rst:53
msgid "Rotating"
msgstr "Rotação"

#: ../../user_manual/getting_started/navigation.rst:53
msgid ""
"You can rotate the canvas without transforming. It can be done with the :kbd:"
"`Ctrl + [` shortcut or :kbd:`4`key  and the other way with :kbd:`Ctrl + ]` "
"shortcut or :kbd:`6` key. Quick mouse based rotation is the :kbd:`Shift + "
"Space` and :kbd:`Shift +` |mousemiddle| shortcuts. To reset rotation use "
"the :kbd:`5` key."
msgstr ""
"Poderá rodar a área de desenho sem a transformar. Poderá ser feito com o :"
"kbd:`Ctrl + [` ou o :kbd:`4` e, da outra forma, como o :kbd:`Ctrl + ]` ou o :"
"kbd:`6`. A rotação rápida com base no rato é com o :kbd:`Shift + Espaço` e "
"o :kbd:`Shift` + |mousemiddle|. Para repor a rotação, use o :kbd:`5`."

#: ../../user_manual/getting_started/navigation.rst:55
msgid "You can also find these under :menuselection:`View --> Canvas`."
msgstr ""
"Também conseguirá encontrar estas opções em :menuselection:`Ver --> Área de "
"Desenho`."

#: ../../user_manual/getting_started/navigation.rst:58
msgid "Dockers"
msgstr "Áreas Acopláveis"

#: ../../user_manual/getting_started/navigation.rst:60
msgid ""
"Krita subdivides many of its options into functional panels called Dockers "
"(also known as Docks)."
msgstr ""
"O Krita sub-divide muitas das suas opções em painéis funcionais chamados de "
"Áreas Acopláveis (ou simplesmente Áreas)."

#: ../../user_manual/getting_started/navigation.rst:62
msgid ""
"Dockers are small windows that can contain, for example, things like the "
"layer stack, Color Palette or list of Brush Presets. Think of them as the "
"painter's palette, or his water, or his brush kit. They can be activated by "
"choosing the :guilabel:`Settings` menu and the :guilabel:`Dockers` sub-menu. "
"There you will find a long list of available options."
msgstr ""
"As áreas acopláveis são pequenas janelas que poderão conter, por exemplo, "
"elementos como a pilha de camadas, a paleta de cores ou as predefinições dos "
"pincéis. Pense nelas como a paleta do pintor, ou o seu kit de pincéis e "
"água. As mesmas poderão ser activadas se escolher o menu de :guilabel:"
"`Configuração` e o seu submenu :guilabel:`Áreas Acopláveis`. Aí poderá "
"encontrar uma grande lista de opções disponíveis."

#: ../../user_manual/getting_started/navigation.rst:64
msgid ""
"Dockers can be removed by clicking the **x** in the upper-right of the "
"docker-window."
msgstr ""
"Poderá remover as áreas acopláveis se carregar no **x** do canto superior "
"direito da janela de áreas acopláveis."

#: ../../user_manual/getting_started/navigation.rst:66
msgid ""
"Dockers, as the name implies, can be docked into the main interface. You can "
"do this by dragging the docker to the sides of the canvas (or top or bottom "
"if you prefer)."
msgstr ""
"As áreas acopláveis, como diz o nome, podem ser acopladas à interface "
"principal. Poderá fazer isso se arrastar a área acoplável aos lados da área "
"de desenho (ou à parte de cima ou baixo, se preferir)."

#: ../../user_manual/getting_started/navigation.rst:68
msgid ""
"Dockers contain many of the \"hidden\", and powerful, aspects of **Krita** "
"that you will want to explore as you start delving deeper into the "
"application."
msgstr ""
"As áreas acopláveis contêm muitos dos aspectos \"escondidos\" e poderosos do "
"**Krita** que irá querer explorar à medida que vai mais fundo na aplicação."

#: ../../user_manual/getting_started/navigation.rst:70
msgid ""
"You can arrange the dockers in almost any permutation and combination "
"according to the needs of your workflow, and then save these arrangements as "
"Workspaces."
msgstr ""
"Poderá organizar as áreas acopláveis  em qualquer combinação ou permutação, "
"de acordo com as necessidades do seu fluxo de trabalho, podendo depois "
"gravar essas organizações como Espaços de Trabalho."

#: ../../user_manual/getting_started/navigation.rst:72
msgid ""
"Dockers can be prevented from docking by pressing the :kbd:`Ctrl` key before "
"starting to drag the docker."
msgstr ""
"Poderá impedir as áreas acopláveis de se acoplarem, carregando para tal em :"
"kbd:`Ctrl`, antes de começar a arrastar a área acoplável."

#: ../../user_manual/getting_started/navigation.rst:75
msgid "Sliders"
msgstr "Barras"

#: ../../user_manual/getting_started/navigation.rst:76
msgid ""
"Krita uses these to control values like brush size, opacity, flow, Hue, "
"Saturation, etc... Below is an example of a Krita slider."
msgstr ""
"O Krita usa-os para controlar os valores como o tamanho do pincel, a "
"opacidade, o fluxo, a Matiz, a Saturação, etc.... Em baixo encontra-se um "
"exemplo de uma barra deslizante do Krita."

#: ../../user_manual/getting_started/navigation.rst:79
msgid ".. image:: images/Krita_Opacity_Slider.png"
msgstr ".. image:: images/Krita_Opacity_Slider.png"

#: ../../user_manual/getting_started/navigation.rst:80
msgid ""
"The total range is represented from left to right and blue bar gives an "
"indication of where in the possible range the current value is. Clicking "
"anywhere, left or right, of that slider will change the current number to "
"something lower (to the left) or higher (to the right)."
msgstr ""
"O intervalo total está representado da esquerda para a direita e a barra "
"azul dá uma indicação de qual é o valor actual no intervalo possível. Se "
"carregar em qualquer lugar, seja à esquerda ou à direita, dessa barra "
"lateral, irá mudar o número actual para algo mais baixo (à esquerda) ou alto "
"(à direita)."

#: ../../user_manual/getting_started/navigation.rst:82
msgid ""
"To input a specific number, |mouseright| the slider. A number can now be "
"entered directly for even greater precision."
msgstr ""
"Para introduzir um número específico, use o |mouseright| sobre a barra "
"deslizante. Poderá agora introduzir um número directamente para obter uma "
"precisão ainda maior."

#: ../../user_manual/getting_started/navigation.rst:84
msgid ""
"Pressing the :kbd:`Shift` key while dragging the slider changes the values "
"at a smaller increment, and pressing the :kbd:`Ctrl` key while dragging the "
"slider changes the value in whole numbers or multiples of 5."
msgstr ""
"Se carregar em :kbd:`Shift` enquanto arrastar a barra, muda os valores com "
"um incremento menor, enquanto se usar o :kbd:`Ctrl` ao arrastar a barra "
"deslizante, irá mudar o valor em valores inteiros ou em múltiplos de 5."

#: ../../user_manual/getting_started/navigation.rst:87
msgid "Toolbars"
msgstr "Barras de Ferramentas"

#: ../../user_manual/getting_started/navigation.rst:89
msgid ".. image:: images/Krita_Toolbar.png"
msgstr ".. image:: images/Krita_Toolbar.png"

#: ../../user_manual/getting_started/navigation.rst:90
msgid ""
"Toolbars are where some of the important actions and menus are placed so "
"that they are readily and quickly available for the artist while painting."
msgstr ""
"As barras de ferramentas são onde estão colocadas algumas das acções e menus "
"principais, para que estejam rápida e prontamente acessíveis ao artista "
"enquanto pinta."

#: ../../user_manual/getting_started/navigation.rst:92
msgid ""
"You can learn more about the Krita Toolbars and how to configure them in "
"over in the :ref:`Toolbars section <configure_toolbars>` of the manual. "
"Putting these to effective use can really speed up the Artist's workflow, "
"especially for users of Tablet-Monitors and Tablet-PCs."
msgstr ""
"Poderá aprender mais sobre as Barras de Ferramentas do Krita e como "
"configurá-las na :ref:`secção de Barras de Ferramentas <configure_toolbars>` "
"do manual. Se as colocar em uso, poderá realmente acelerar o trabalho do "
"artista, especialmente com os utilizadores dos Monitores-Tabletes e dos PC's "
"com Tabletes."

#: ../../user_manual/getting_started/navigation.rst:96
msgid "Workspace Chooser"
msgstr "Selector de Espaços de Trabalho"

#: ../../user_manual/getting_started/navigation.rst:98
msgid ""
"The button on the very right of the Toolbar is the workspace chooser. This "
"allows you to load and save common configurations of the user interface in "
"Krita. There are a few common workspaces that come with Krita."
msgstr ""
"O botão no extremo direito da barra de ferramentas é o selector de espaços "
"de trabalho. Isto permite-lhe carregar e gravar configurações comuns da "
"interface do utilizador do Krita. Existe alguns espaços de trabalho comuns "
"que vêm com o Krita."

#: ../../user_manual/getting_started/navigation.rst:101
msgid ".. image:: images/workspace-chooser-button.svg"
msgstr ".. image:: images/workspace-chooser-button.svg"

#: ../../user_manual/getting_started/navigation.rst:108
msgid ""
":ref:`Pop-up Palette <pop-up_palette>` is a feature unique to Krita, "
"designed to increase the productivity of the artist. It is a circular menu "
"for quickly choosing brushes, foreground and background colors, recent "
"colors while painting. To access the palette you have to just |mouseright| "
"on the canvas. The palette will spawn at the position of the brush tip or "
"cursor."
msgstr ""
"A :ref:`Paleta Instantânea <pop-up_palette>` é uma funcionalidade única no "
"Krita que aumenta a produtividade do artista. É um menu circular para "
"escolher rapidamente os pincéis, as cores de fundo ou principais, bem como "
"as cores recentes, ao pintar. Para aceder à paleta, terá apenas de usar o |"
"mouseright| sobre a área de desenho. A paleta irá aparecer no local da ponta "
"do pincel ou do cursor."

#: ../../user_manual/getting_started/navigation.rst:110
msgid ""
"By tagging your brush presets you can add particular sets of brushes to this "
"palette. For example, if you add some inking brush presets to inking tag you "
"can change the tags to inking in the pop-up palette and you'll get all the "
"inking brushes in the palette."
msgstr ""
"Ao atribuir marcas às predefinições dos seus pincéis, poderá adicionar "
"conjuntos particulares de pincéis a esta paleta. Por exemplo, se adicionar "
"algumas predefinições de pincéis à marca de pintura, poderá mudar as marcas "
"na pintura na paleta e irá obter todos os pincéis de pintura na paleta."

#: ../../user_manual/getting_started/navigation.rst:112
msgid ""
"You can :ref:`tag <tag_management>` brush presets via the :ref:"
"`brush_preset_docker`, check out the :ref:`resource overview page "
"<resource_management>` to know more about tagging in general."
msgstr ""
"Poderá :ref:`marcar <tag_management>` as predefinições de pincéis através "
"da :ref:`brush_preset_docker`; consulte a página de introdução aos recursos "
"para saber mais sobre as marcações de um modo geral."

#: ../../user_manual/getting_started/navigation.rst:114
msgid ""
"If you call up the pop-up palette again, you can click the tag icon, and "
"select the tag. In fact, you can make multiple tags and switch between them. "
"When you need more than ten presets, go into :menuselection:`Settings --> "
"Configure Krita --> General --> Miscellaneous --> Number of Palette Presets` "
"and change the number of presets from 10 to something you feel comfortable."
msgstr ""
"Se invocar a paleta de novo, poderá carregar no ícone da engrenagem e "
"seleccionar a marca. De facto, poderá criar várias marcas e alternar entre "
"elas. Quando precisar de mais que as 10 predefinições, vá a :menuselection:"
"`Configuração --> Configurar o Krita --> Geral --> Predefinições favoritas` "
"e mude o número de predefinições de 10 para outro valor com que se sinta "
"mais confortável."
