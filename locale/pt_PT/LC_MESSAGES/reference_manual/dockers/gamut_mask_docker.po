# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 09:55+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: images image Kritamouseleft alt icons dockers\n"
"X-POFile-SpellExtra: vectorgraphics guilabel YouTube Gourney Gamute\n"
"X-POFile-SpellExtra: maskShapesLayer Gurney KritaGamutMaskDocker ref\n"
"X-POFile-SpellExtra: mouseleft\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:1
msgid "Overview of the gamut mask docker."
msgstr "Introdução à área de máscara do gamute."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:11
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:11
msgid "Color Selector"
msgstr "Selecção de Cores"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:11
msgid "Gamut Mask Docker"
msgstr "Área da Ḿáscara do Gamute."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:11
msgid "Gamut Mask"
msgstr "Ḿáscara do Gamute"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:16
msgid "Gamut Masks Docker"
msgstr "Área de Ḿáscaras do Gamute"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:19
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:22
msgid "Docker for gamut masks selection and management."
msgstr "Área acoplável para a selecção e gestão de máscaras do gamute."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:25
msgid "Usage"
msgstr "Utilização"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:27
msgid "|mouseleft| an icon (1) to apply a mask to color selectors."
msgstr ""
"Use o |mouseleft| sobre um ícone (1) para aplicar uma máscara aos selectores "
"de cores."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:29
msgid "Gamut Masks can be imported and exported in the resource manager."
msgstr ""
"As Máscaras de Gamute podem ser importadas e exportadas no gestor de "
"recursos."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:33
msgid "Management Toolbar"
msgstr "Barra de Gestão"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:35
msgid "Create new mask (2)"
msgstr "Criar uma nova máscara (2)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:36
msgid "Opens the mask editor with an empty template."
msgstr "Abre o editor da máscara com um modelo em branco."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:37
msgid "Edit mask (3)"
msgstr "Editar a máscara (3)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:38
msgid "Opens the the currently selected mask in the editor."
msgstr "Abre a máscara seleccionada de momento no editor."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:39
msgid "Duplicate mask (4)"
msgstr "Duplicar a máscara (4)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:40
msgid ""
"Creates a copy of the currently selected mask and opens the copy in the "
"editor."
msgstr ""
"Cria uma cópia da máscara seleccionada de momento e abre a cópia no editor."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:42
msgid "Deletes the currently selected mask."
msgstr "Apaga a máscara seleccionada de momento."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:43
msgid "Delete mask (5)"
msgstr "Apagar a máscara (5)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:46
msgid "Editing"
msgstr "Edição"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:48
msgid ""
"If you choose to create a new mask, edit, or duplicate selected mask, the "
"mask template document will be opened as a new view (1)."
msgstr ""
"Se optar por criar uma nova máscara, editar ou duplicar a máscara "
"seleccionada, o documento do modelo da máscara será aberto numa nova vista "
"(1)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:50
msgid ""
"There you can create new shapes and modify the mask with standard vector "
"tools (:ref:`vector_graphics`)."
msgstr ""
"Aí poderá criar novas formas e modificar a máscara com ferramentas "
"vectoriais normais (:ref:`vector_graphics`)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:52
msgid "Fill in the fields at (2)."
msgstr "Preencha os campos em (2)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:54
msgid "Title (Mandatory)"
msgstr "Título (Obrigatório)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:55
msgid "The name of the gamut mask."
msgstr "O nome da máscara de gamute."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:57
msgid "Description"
msgstr "Descrição"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:57
msgid "A description."
msgstr "Uma descrição."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:59
msgid ""
":guilabel:`Preview` the mask in the artistic color selector (4), :guilabel:"
"`save` the mask (5), or :guilabel:`cancel` editing (3)."
msgstr ""
"Pode aceder à :guilabel:`Antevisão` da máscara no selector de cores "
"artísticas (4), :guilabel:`Gravar` a máscara (5) ou :guilabel:`Cancelar` a "
"edição (3)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:63
msgid ""
"The shapes need to be added to the layer named “maskShapesLayer” (which is "
"selected by default)."
msgstr ""
"As formas precisam de ser adicionadas à camada chamada "
"“maskShapesLayer” (que está seleccionada por omissão)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:64
msgid "The shapes need have solid background to show correctly in the editor."
msgstr ""
"As formas precisam de ter um fundo com cor única para aparecer correctamente "
"no editor."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:65
msgid "A template with no shapes cannot be saved."
msgstr "Não é possível gravar um modelo sem formas."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:69
msgid ""
"The mask is intended to be composed of basic vector shapes. Although "
"interesting results might arise from using advanced vector drawing "
"techniques, not all features are guaranteed to work properly (e.g. grouping, "
"vector text, etc.)."
msgstr ""
"A máscara pretende ser composta por formas vectoriais básicas. Ainda que "
"possam surgir resultados interessantes da utilização de técnicas de desenho "
"vectorial avançado, nem todas as funcionalidades são garantidas que "
"funcionem correctamente (p.ex. agrupamento, texto vectorial, etc.)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:72
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:74
msgid "External Info"
msgstr "Informação Externa"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:76
msgid ""
"`Color Wheel Masking, Part 1 by James Gurney. <https://gurneyjourney."
"blogspot.com/2008/01/color-wheel-masking-part-1.html>`_"
msgstr ""
"`Máscara da Roda de Cores, Parte 1 de James Gurney. <https://gurneyjourney."
"blogspot.com/2008/01/color-wheel-masking-part-1.html>`_"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:77
msgid ""
"`The Shapes of Color Schemes by James Gurney. <https://gurneyjourney."
"blogspot.com/2008/02/shapes-of-color-schemes.html>`_"
msgstr ""
"`As Formas dos Esquemas de Cores de James Gurney. <https://gurneyjourney."
"blogspot.com/2008/02/shapes-of-color-schemes.html>`_"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:78
msgid ""
"`Gamut Masking Demonstration by James Gourney (YouTube). <https://youtu.be/"
"qfE4E5goEIc>`_"
msgstr ""
"`Demonstração da Máscara do Gamute de James Gourney (YouTube). <https://"
"youtu.be/qfE4E5goEIc>`_"
