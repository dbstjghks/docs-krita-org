msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___tools___outline_select.pot\n"

#: ../../<generated>:1
msgid "Anti-aliasing"
msgstr "抗锯齿"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""

#: ../../<rst_epilog>:74
msgid ""
".. image:: images/icons/outline_select_tool.svg\n"
"   :alt: toolselectoutline"
msgstr ""
".. image:: images/icons/outline_select_tool.svg\n"
"   :alt: toolselectoutline"

#: ../../reference_manual/tools/outline_select.rst:1
msgid "Krita's outline selection tool reference."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:13
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:13
msgid "Selection"
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:13
msgid "Freehand"
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:13
msgid "Outline Select"
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:18
msgid "Outline Selection Tool"
msgstr "轮廓选区工具"

#: ../../reference_manual/tools/outline_select.rst:20
msgid "|toolselectoutline|"
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:22
msgid ""
"Make :ref:`selections_basics` by drawing freehand around the canvas. Click "
"and drag to draw a border around the section you wish to select."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:25
msgid "Hotkeys and Sticky keys"
msgstr "快捷键和粘滞键"

#: ../../reference_manual/tools/outline_select.rst:27
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:28
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:29
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:30
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:31
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:32
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:33
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:37
msgid "Hovering over a selection allows you to move it."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:38
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:42
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:45
msgid "Tool Options"
msgstr "工具选项"

#: ../../reference_manual/tools/outline_select.rst:48
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
