# Translation of docs_krita_org_general_concepts___projection___practical.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___projection___practical\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:48+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../general_concepts/projection/practical.rst:None
msgid ".. image:: images/category_projection/projection_animation_04.gif"
msgstr ".. image:: images/category_projection/projection_animation_04.gif"

#: ../../general_concepts/projection/practical.rst:None
msgid ".. image:: images/category_projection/projection_image_38.png"
msgstr ".. image:: images/category_projection/projection_image_38.png"

#: ../../general_concepts/projection/practical.rst:None
msgid ".. image:: images/category_projection/projection_animation_05.gif"
msgstr ".. image:: images/category_projection/projection_animation_05.gif"

#: ../../general_concepts/projection/practical.rst:None
msgid ".. image:: images/category_projection/projection_image_39.png"
msgstr ".. image:: images/category_projection/projection_image_39.png"

#: ../../general_concepts/projection/practical.rst:None
msgid ".. image:: images/category_projection/projection_animation_06.gif"
msgstr ".. image:: images/category_projection/projection_animation_06.gif"

#: ../../general_concepts/projection/practical.rst:None
msgid ".. image:: images/category_projection/projection_image_40.png"
msgstr ".. image:: images/category_projection/projection_image_40.png"

#: ../../general_concepts/projection/practical.rst:1
msgid "Practical uses of perspective projection."
msgstr "Практичне використання проєкції перспективи."

#: ../../general_concepts/projection/practical.rst:10
msgid ""
"This is a continuation of the :ref:`perspective projection tutorial "
"<projection_perspective>`, be sure to check it out if you get confused!"
msgstr ""
"Це продовження :ref:`підручника щодо проєкції перспективи "
"<projection_perspective>`. Прочитайте його спершу, якщо щось у цьому розділі "
"видасться вам незрозумілим!"

#: ../../general_concepts/projection/practical.rst:12
msgid "Projection"
msgstr "Проєкція"

#: ../../general_concepts/projection/practical.rst:12
msgid "Perspective"
msgstr "Перспектива"

#: ../../general_concepts/projection/practical.rst:16
msgid "Practical"
msgstr "Практика"

#: ../../general_concepts/projection/practical.rst:18
msgid ""
"So, if computers can already automate a ton, and it is fairly complicated, "
"is there still a use for a traditional 2d artist to learn this?"
msgstr ""
"Отже, комп'ютер вже можуть багато усього оптимізувати, а тема ця доволі "
"складна. То чи є сенс усе це вивчати для традиційних плоских малюнків?"

#: ../../general_concepts/projection/practical.rst:20
msgid ""
"Yes, actually. The benefit that 2d art still has over 3d is that it's plain "
"faster for single images, especially with complicated subjects like faces "
"and bodies."
msgstr ""
"Так, є. Перевагою плоского малюнка над просторовою моделлю є те, що "
"намалювати плоский малюнок набагато швидше, особливо для складних речей, "
"зокрема облич або тіл."

#: ../../general_concepts/projection/practical.rst:22
msgid ""
"Perspective projection can help a lot getting down those annoying poses, "
"like people lying down. It also helps when combining 2d and 3d, as when you "
"know where the camera is in the 3d render, you can use that in a projection "
"to get the character projected."
msgstr ""
"Проєкція перспективи може допомогти у малюванні усіх цих складних поз, "
"зокрема людей, які лягають. Також вона допомагає поєднувати плоскі і "
"просторові частини зображення. Наприклад, коли ви знаєте, де розташовано "
"камеру у просторовій системі обробки зображення, ви можете скористатися цим "
"для малювання проєкцій персонажів."

#: ../../general_concepts/projection/practical.rst:27
msgid ""
"The side view of a person lying down is often easy to draw, but the top view "
"or the view from the feet isn’t. Hence why we use the side view to do "
"perspective projection on."
msgstr ""
"Намалювати людину, яка лежить, збоку часто доволі просто, але не так просто "
"намалювати її згори або з боку ніг. Тому ми можемо скористатися бічною "
"проєкцією для побудови проєкції перспективи."

#: ../../general_concepts/projection/practical.rst:32
msgid "Another example with an equally epic task: sitting."
msgstr "Ще один приклад з таким самим епічним завданням: сидіння."

#: ../../general_concepts/projection/practical.rst:37
msgid ""
"Now, with this one we have a second vanishing point above the front-view. It "
"should be about the same distance above the front-view as it is above the "
"head of the rotated side-view. The projection plane should also be the same "
"distance from the vanishing point, but that doesn’t mean it has to be "
"behind. This is something I avoided in the earlier examples, because it "
"makes the working field really messy, but if you look up perspective "
"projection you’ll see multiple examples of this method."
msgstr ""
"Тепер, тут ми маємо другу нескінченно віддалену точку над видом спереду. "
"Вона має бути приблизно на тій самій відстані над видом спереду, що і над "
"головою у повернутому виді збоку. Крім того, площина проєктування має бути "
"на тій самій відстані від нескінченно віддаленої точки, але це не значить, "
"що вона має бути ззаду. Це те, чого ми уникали у попередніх прикладах, "
"оскільки це робить робоче поле для малювання захаращеним, але якщо ви "
"поглянете на проєкцію перспективи у цих прикладає, ви побачите декілька "
"застосувань цього методу."

#: ../../general_concepts/projection/practical.rst:39
msgid ""
"Also of note is that you actually should be having the view plane/projection "
"plane perfectly perpendicular to the angle of the focal point, otherwise you "
"get odd distortion, this doesn’t happen here, which means this sitting "
"person is a bit more stretched vertically than necessary."
msgstr ""
"Також слід зауважити, що площина перегляду або площина проєкції на нашому "
"малюнку має бути ідеально перпендикулярна до напрямку погляду з фокальної "
"точки, інакше ми отримаємо небажане викривлення. Такого у нас не трапилося, "
"що означає, що персонаж, який сидить, є трохи вигянутішим вертикально, ніж "
"це потрібно."

#: ../../general_concepts/projection/practical.rst:44
msgid "One more, for the road…"
msgstr "Ще один малюнок для дороги…"

#: ../../general_concepts/projection/practical.rst:49
msgid ""
"Here you can see that the misalignment of the vanishing point to the "
"projection plane causes skewing which was then fixed by Krita’s transform "
"tools, technically it’s of course correct, but what is correct doesn’t "
"always look good. (I also mess up the position of the shoulder for a good "
"while if you look closely.)"
msgstr ""
"Тут ви можете бачити, що невирівняність нескінченно віддаленої точки щодо "
"площини проєктування спричиняє перекошування, яке потім виправляється за "
"допомогою засобів перетворення Krita. Технічно, це, звичайно ж, правильно, "
"але те, що правильно, не завжди добре виглядає. (Якщо ви подивитеся "
"уважніше, автор дещо змінив розташування плеча, щоб усе виглядало краще.)"

#: ../../general_concepts/projection/practical.rst:55
msgid "Conclusion and afterthoughts"
msgstr "Висновки і додаткові міркування"

#: ../../general_concepts/projection/practical.rst:57
msgid ""
"I probably didn’t make as nice result images as I could have, especially if "
"you compare it to the 3d images. However, you can still see that the main "
"landmarks are there. The real use of this technique lies in poses though, "
"and it allows you to iterate on a pose quite quickly once you get the hang "
"of it."
msgstr ""
"Ймовірно, зображення не такі красиві, як могли б бути, особливо якщо ви "
"порівняєте їх з результатами обробки просторових моделей. Втім, вони корисні "
"для вивчення методики. Реальне використання цієї методики полягає у позах. "
"Вона надає вам змогу виконувати ітерацію за позами доволі швидко, щойно ви "
"оволодієте нею."

#: ../../general_concepts/projection/practical.rst:59
msgid ""
"Generally, it’s worth exploring, if only because it improves your spatial "
"sense."
msgstr ""
"Загалом кажучи, цю тему варто вивчити, хоча б задля поліпшення просторової "
"уяви."

#: ../../general_concepts/projection/practical.rst:63
msgid "https://en.wikipedia.org/wiki/Axonometric_projection"
msgstr "https://en.wikipedia.org/wiki/Axonometric_projection"

#: ../../general_concepts/projection/practical.rst:64
msgid "https://blenderartists.org/t/creating-an-isometric-camera/440743"
msgstr "https://blenderartists.org/t/creating-an-isometric-camera/440743"

#: ../../general_concepts/projection/practical.rst:65
msgid "http://flarerpg.org/tutorials/isometric_tiles/"
msgstr "http://flarerpg.org/tutorials/isometric_tiles/"

#: ../../general_concepts/projection/practical.rst:66
msgid ""
"https://en.wikipedia.org/wiki/Isometric_graphics_in_video_games_and_pixel_art"
msgstr ""
"https://en.wikipedia.org/wiki/Isometric_graphics_in_video_games_and_pixel_art"

#: ../../general_concepts/projection/practical.rst:67
msgid "https://en.wikipedia.org/wiki/Lens_%28optics%29"
msgstr "https://en.wikipedia.org/wiki/Lens_%28optics%29"
