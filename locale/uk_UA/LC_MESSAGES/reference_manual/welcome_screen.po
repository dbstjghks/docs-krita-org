# Translation of docs_krita_org_reference_manual___welcome_screen.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___welcome_screen\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:27+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/welcome_screen.rst:None
msgid ".. image:: images/welcome_screen.png"
msgstr ".. image:: images/welcome_screen.png"

#: ../../reference_manual/welcome_screen.rst:1
msgid "The welcome screen in Krita."
msgstr "Вікно вітання у Krita."

#: ../../reference_manual/welcome_screen.rst:10
#: ../../reference_manual/welcome_screen.rst:14
msgid "Welcome Screen"
msgstr "Вікно вітання"

#: ../../reference_manual/welcome_screen.rst:16
msgid ""
"When you open Krita, starting from version 4.1.3, you will be greeted by a "
"welcome screen. This screen makes it easy for you to get started with Krita, "
"as it provides a collection of shortcuts for the most common tasks that you "
"will probably be doing when you open Krita."
msgstr ""
"Після запуску Krita, починаючи з версії 4.1.3, ви побачите вікно вітання. Це "
"вікно спрощує початкові дії у Krita, оскільки у ньому наведено збірку "
"посилань на типові завдання, які, ймовірно, виконує кожен користувач після "
"запуску Krita."

#: ../../reference_manual/welcome_screen.rst:23
msgid "The screen is divided into 4 sections:"
msgstr "Вікно поділено на 4 розділи:"

#: ../../reference_manual/welcome_screen.rst:25
msgid ""
"The :guilabel:`Start` section there are links to create new document as well "
"to open an existing document."
msgstr ""
"У розділі :guilabel:`Початок` буде наведено посилання на створення "
"документа, а також посилання на відкриття наявних документів."

#: ../../reference_manual/welcome_screen.rst:28
msgid ""
"The :guilabel:`Recent Documents` section has a list of recently opened "
"documents from your previous sessions in Krita."
msgstr ""
"У розділі :guilabel:`Недавні документи` буде наведено список нещодавно "
"відкритих документів з попереднього сеансу роботи у Krita."

#: ../../reference_manual/welcome_screen.rst:31
msgid ""
"The :guilabel:`Community` section provides some links to get help, "
"Supporting development of Krita, Source code of Krita and to links to "
"interact with our user community."
msgstr ""
"У розділі :guilabel:`Спільнота` буде наведено посилання на сторінки "
"отримання допомогти, підтримки розробки Krita, програмного коду Krita та "
"посилання для взаємодії зі спільнотою користувачів."

#: ../../reference_manual/welcome_screen.rst:35
msgid ""
"The :guilabel:`News` section, which is disabled by default, when enabled "
"provides you with latest news feeds fetched from Krita website, this will "
"help you stay up to date with the release news and other events happening in "
"our community."
msgstr ""
"У розділі :guilabel:`Новини`, який типово вимкнено, якщо його увімкнено буде "
"показано найсвіжіші новини, які отримано з сайта Krita. За допомогою цього "
"розділу ви зможете тримати руку на пульсі подій щодо випусків та інших новин "
"спільноти."

#: ../../reference_manual/welcome_screen.rst:39
msgid ""
"Other than the above sections the welcome screen also acts as a drop area "
"for opening any document. You just have to drag and drop a Krita document or "
"any supported image files on the empty area around the sections to open it "
"in Krita."
msgstr ""
"Окрім показу вказаних вище розділів, вікно вітання слугує областю скидання "
"для відкриття документів. Достатньо просто перетягнути і скинути на порожнє "
"місце поза розділами документ Krita або зображення у іншому підтримуваному "
"форматі, щоб відповідні дані було відкрито у Krita."
