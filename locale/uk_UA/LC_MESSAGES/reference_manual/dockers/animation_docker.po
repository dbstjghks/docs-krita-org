# Translation of docs_krita_org_reference_manual___dockers___animation_docker.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___animation_docker\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-05 08:02+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/dockers/animation_docker.rst:1
msgid "Overview of the animation docker."
msgstr "Огляд бічної панелі анімації."

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Animation"
msgstr "Анімація"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Animation Playback"
msgstr "Відтворення анімації"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Play/Pause"
msgstr "Пуск/Пауза"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Framerate"
msgstr "Частота кадрів"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "FPS"
msgstr "Кадри за секунду"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Speed"
msgstr "Швидкість"

#: ../../reference_manual/dockers/animation_docker.rst:15
msgid "Animation Docker"
msgstr "Бічна панель анімації"

#: ../../reference_manual/dockers/animation_docker.rst:18
msgid ".. image:: images/dockers/Animation_docker.png"
msgstr ".. image:: images/dockers/Animation_docker.png"

#: ../../reference_manual/dockers/animation_docker.rst:19
msgid ""
"To have a playback of the animation, you need to use the animation docker."
msgstr ""
"Для відтворення анімації вам слід скористатися бічною панеллю анімації."

#: ../../reference_manual/dockers/animation_docker.rst:21
msgid ""
"The first big box represents the current Frame. The frames are counted with "
"programmer's counting so they start at 0."
msgstr ""
"Перший великий стовпчик відповідає поточному кадру. Відлік кадрів відповідає "
"відліку у програмуванні, тому починається з нуля."

#: ../../reference_manual/dockers/animation_docker.rst:23
msgid ""
"Then there are two boxes for you to change the playback range here. So, if "
"you want to do a 10 frame animation, set the end to 10, and then Krita will "
"cycle through the frames 0 to 10."
msgstr ""
"Далі, існує два поля для внесення змін до діапазону відтворення. Отже, якщо "
"ви хочете створити анімацію з 10 кадрів, встановіть для кінця значення 10. "
"Krita виконує циклічний перехід кадрами від 0 до 10."

#: ../../reference_manual/dockers/animation_docker.rst:25
msgid ""
"The bar in the middle is filled with playback options, and each of these can "
"also be hot-keyed. The difference between a keyframe and a normal frame in "
"this case is that a normal frame is empty, while a keyframe is filled."
msgstr ""
"Панель посередині заповнено параметрами відтворення, кожен з яких можна "
"пов'язати із клавіатурними скороченнями. Відмінність між ключовим кадром і "
"звичайним кадром у цьому випадку полягає у тому, що звичайний кадр є "
"порожнім, а ключовий кадр — заповненим."

#: ../../reference_manual/dockers/animation_docker.rst:27
msgid ""
"Then, there's buttons for adding, copying and removing frames. More "
"interesting is the next row:"
msgstr ""
"Далі, передбачено кнопки для додавання, копіювання та вилучення кадрів. "
"Цікавішим є наступний рядок:"

#: ../../reference_manual/dockers/animation_docker.rst:29
msgid "Onion Skin"
msgstr "Калька"

#: ../../reference_manual/dockers/animation_docker.rst:30
msgid "Opens the :ref:`onion_skin_docker` if it wasn't open before."
msgstr ""
"Відкриває :ref:`onion_skin_docker`, якщо цю панель ще не було відкрито."

#: ../../reference_manual/dockers/animation_docker.rst:31
msgid "Auto Frame Mode"
msgstr "Режим автоматичного кадрування"

#: ../../reference_manual/dockers/animation_docker.rst:32
msgid ""
"Will make a frame out of any empty frame you are working on. Currently "
"automatically copies the previous frame."
msgstr ""
"Створює кадр з будь-якого порожнього кадру, над яким ви працюєте. У поточній "
"версії автоматично копіює попередній кадр."

#: ../../reference_manual/dockers/animation_docker.rst:34
msgid "Drop frames"
msgstr "Пропускання кадрів"

#: ../../reference_manual/dockers/animation_docker.rst:34
msgid ""
"This'll drop frames if your computer isn't fast enough to show all frames at "
"once. This process is automatic, but the icon will become red if it's forced "
"to do this."
msgstr ""
"За допомогою цього пункту можна наказати комп'ютеру пропускати кадри, якщо "
"йому не вистачає потужності показувати їх. Цей процес є автоматичним, але "
"відповідну піктограму буде показано червоним кольором, якщо пропускання "
"кадрів є примусовим."

#: ../../reference_manual/dockers/animation_docker.rst:36
msgid ""
"You can also set the speedup of the playback, which is different from the "
"framerate."
msgstr ""
"Крім того, ви можете встановити швидкість відтворення, яка відрізнятиметься "
"від частоти кадрів."
