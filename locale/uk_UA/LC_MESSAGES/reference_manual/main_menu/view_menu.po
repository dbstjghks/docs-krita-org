# Translation of docs_krita_org_reference_manual___main_menu___view_menu.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___main_menu___view_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:41+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Show Painting Previews"
msgstr "Показ мініатюр малювання"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/main_menu/view_menu.rst:1
msgid "The view menu in Krita."
msgstr "Меню перегляду у Krita."

#: ../../reference_manual/main_menu/view_menu.rst:11
msgid "View"
msgstr "Перегляд"

#: ../../reference_manual/main_menu/view_menu.rst:11
msgid "Wrap around mode"
msgstr "Режим огортання"

#: ../../reference_manual/main_menu/view_menu.rst:16
msgid "View Menu"
msgstr "Меню «Перегляд»"

#: ../../reference_manual/main_menu/view_menu.rst:18
msgid "Show Canvas Only"
msgstr "Показувати лише полотно"

#: ../../reference_manual/main_menu/view_menu.rst:19
msgid ""
"Only shows the canvas and what you have configured to show in :guilabel:"
"`Canvas Only` settings."
msgstr ""
"Показувати лише полотно і те, що налаштовано у параметрах режиму показу лише "
"полотна."

#: ../../reference_manual/main_menu/view_menu.rst:20
msgid "Fullscreen mode"
msgstr "Повноекранний режим"

#: ../../reference_manual/main_menu/view_menu.rst:21
msgid "This will hide the system bar."
msgstr "Приховати системну панель."

#: ../../reference_manual/main_menu/view_menu.rst:22
msgid "Wrap Around Mode"
msgstr "Режим огортання"

#: ../../reference_manual/main_menu/view_menu.rst:23
msgid ""
"This will show the image as if tiled orthographically. Very useful for "
"tiling 3d textures. Hit the :kbd:`W` key to quickly activate it."
msgstr ""
"У цьому режимі програма показуватиме зображення так, наче його поділено на "
"плитки перпендикулярними лініями. Такий режим є дуже корисним для поділу на "
"плитку просторових текстур. Натисніть клавішу :kbd:`W`, щоб швидко "
"активувати цей режим."

#: ../../reference_manual/main_menu/view_menu.rst:24
msgid "Instant Preview"
msgstr "Швидкий перегляд"

#: ../../reference_manual/main_menu/view_menu.rst:25
msgid "Toggle :ref:`instant_preview` globally."
msgstr "Увімкнути або вимкнути :ref:`instant_preview` на загальному рівні."

#: ../../reference_manual/main_menu/view_menu.rst:26
msgid "Soft Proofing"
msgstr "Проба кольорів"

#: ../../reference_manual/main_menu/view_menu.rst:27
msgid "Activate :ref:`soft_proofing`."
msgstr "Задіяти :ref:`soft_proofing`."

#: ../../reference_manual/main_menu/view_menu.rst:28
msgid "Out of Gamut Warnings"
msgstr "Попередження для кольорів поза гамою"

#: ../../reference_manual/main_menu/view_menu.rst:29
msgid "See the :ref:`soft_proofing` page for details."
msgstr "Див. сторінку :ref:`soft_proofing`, щоб дізнатися більше."

#: ../../reference_manual/main_menu/view_menu.rst:30
msgid "Canvas"
msgstr "Полотно"

#: ../../reference_manual/main_menu/view_menu.rst:31
msgid "Contains view manipulation actions."
msgstr "Містить пункти дій із керування переглядом."

#: ../../reference_manual/main_menu/view_menu.rst:32
msgid "Mirror View"
msgstr "Віддзеркалити перегляд"

#: ../../reference_manual/main_menu/view_menu.rst:33
msgid ""
"This will mirror the view. Hit the :kbd:`M` key to quickly activate it. Very "
"useful during painting."
msgstr ""
"За допомогою цього пункту можна віддзеркалити перегляд. Натисніть клавішу :"
"kbd:`M`, щоб швидко активувати цю дію. Дуже корисно під час малювання."

#: ../../reference_manual/main_menu/view_menu.rst:34
msgid "Show Rulers"
msgstr "Показати лінійки"

#: ../../reference_manual/main_menu/view_menu.rst:35
msgid ""
"This will display a set of rulers. |mouseright| the rulers after showing "
"them, to change the units."
msgstr ""
"Показати набір лінійок. Змінити одиниці виміру на лінійках можна за "
"допомогою контекстного меню, яке викликається клацанням |mouseright|."

#: ../../reference_manual/main_menu/view_menu.rst:36
msgid "Rulers track pointer"
msgstr "Лінійки стежать за вказівником"

#: ../../reference_manual/main_menu/view_menu.rst:37
msgid ""
"This adds a little marker to the ruler to show where the mouse is in "
"relation to them."
msgstr ""
"Додати на лінійку маленьку позначку, яка показуватиме, де розташовано "
"вказівник миші відносно лінійки."

#: ../../reference_manual/main_menu/view_menu.rst:38
msgid "Show Guides"
msgstr "Показати напрямні"

#: ../../reference_manual/main_menu/view_menu.rst:39
msgid "Show or hide the guides."
msgstr "Показати або приховати напрямні."

#: ../../reference_manual/main_menu/view_menu.rst:40
msgid "Lock Guides"
msgstr "Заблокувати напрямні"

#: ../../reference_manual/main_menu/view_menu.rst:41
msgid "Prevent the guides from being able to be moved by the cursor."
msgstr "Запобігати можливому пересуванню напрямних за допомогою вказівника."

#: ../../reference_manual/main_menu/view_menu.rst:42
msgid "Show Status Bar"
msgstr "Показати смужку стану"

#: ../../reference_manual/main_menu/view_menu.rst:43
msgid ""
"This will show the status bar. The status bar contains a lot of important "
"information, a zoom widget, and the button to switch Selection Display Mode."
msgstr ""
"Показати смужку стану. На смужці стану програма показує важливі дані, віджет "
"масштабування та кнопку для перемикання режиму показу позначеної ділянки."

#: ../../reference_manual/main_menu/view_menu.rst:44
msgid "Show Grid"
msgstr "Показати ґратку"

#: ../../reference_manual/main_menu/view_menu.rst:45
msgid "Shows and hides the grid. Shortcut: :kbd:`Ctrl + Shift + '`"
msgstr "Показати або приховати ґратку. :kbd:`Ctrl + Shift + '`"

#: ../../reference_manual/main_menu/view_menu.rst:46
msgid "Show Pixel Grid"
msgstr "Показати піксельну сітку"

#: ../../reference_manual/main_menu/view_menu.rst:47
msgid "Show the pixel grid as configured in the :ref:`display_settings`."
msgstr ""
"Показати піксельну ґратку відповідно до налаштувань у розділі :ref:"
"`display_settings`."

#: ../../reference_manual/main_menu/view_menu.rst:48
msgid "Snapping"
msgstr "Прилипання"

#: ../../reference_manual/main_menu/view_menu.rst:49
msgid "Toggle the :ref:`snapping` types."
msgstr "Увімкнути або вимкнути типи :ref:`snapping`."

#: ../../reference_manual/main_menu/view_menu.rst:50
msgid "Show Painting Assistants"
msgstr "Показати допоміжні засоби малювання"

#: ../../reference_manual/main_menu/view_menu.rst:51
msgid "Shows or hides the Assistants."
msgstr "Показати або приховати допоміжні засоби."

#: ../../reference_manual/main_menu/view_menu.rst:53
msgid "Shows or hides the Previews."
msgstr "Показати або приховати панелі попереднього перегляду."
