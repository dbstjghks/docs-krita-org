# Translation of docs_krita_org_reference_manual___tools___path.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___path\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:45+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Activate Angle Snap"
msgstr "Задіяння прилипання під кутом"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../<rst_epilog>:34
msgid ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: toolbeziercurve"
msgstr ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: toolbeziercurve"

#: ../../reference_manual/tools/path.rst:1
msgid "Krita's path tool reference."
msgstr "Довідник із інструмента контурів Krita."

#: ../../reference_manual/tools/path.rst:11
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/path.rst:11
msgid "Vector"
msgstr "Вектор"

#: ../../reference_manual/tools/path.rst:11
msgid "Path"
msgstr "Контур"

#: ../../reference_manual/tools/path.rst:11
msgid "Bezier Curve"
msgstr "Крива Безьє"

#: ../../reference_manual/tools/path.rst:11
msgid "Pen"
msgstr "Перо"

#: ../../reference_manual/tools/path.rst:17
msgid "Bezier Curve Tool"
msgstr "Інструмент «Крива Безьє»"

#: ../../reference_manual/tools/path.rst:19
msgid "|toolbeziercurve|"
msgstr "|toolbeziercurve|"

#: ../../reference_manual/tools/path.rst:21
msgid ""
"You can draw curves by using this tool. Click the |mouseleft| to indicate "
"the starting point of the curve, then click again for consecutive control "
"points of the curve."
msgstr ""
"За допомогою цього інструмента можна малювати криві. Клацніть |mouseleft|, "
"щоб позначити початкову точку кривої, потім клацайте далі, щоб вказати "
"наступні керівні точки кривої."

#: ../../reference_manual/tools/path.rst:23
msgid ""
":program:`Krita` will show a blue line with two handles when you add a "
"control point. You can drag these handles to change the direction of the "
"curve in that point."
msgstr ""
"Коли ви додасте контрольну точку, :program:`Krita` покаже синю лінію із "
"двома елементами керування. Ви можете перетягти ці елементи керування, щоб "
"змінити напрямок кривої у відповідній точці."

#: ../../reference_manual/tools/path.rst:25
msgid ""
"On a vector layer, you can click on a previously inserted control point to "
"modify it. With an intermediate control point (i.e. a point that is not the "
"starting point and not the ending point), you can move the direction handles "
"separately to have the curve enter and leave the point in different "
"directions. After editing a point, you can just click on the canvas to "
"continue adding points to the curve."
msgstr ""
"У векторному шарі ви можете клацнути на попередньо вставленій контрольній "
"точці, щоб внести зміни до її параметрів. Якщо контрольна точка є проміжною "
"(тобто не є ні початковою, ні кінцевою), ви можете пересувати елементи "
"керування напрямком так, щоб крива проходила крізь точку у різних напрямках. "
"Після редагування точки ви можете продовжити клацання на полотні для "
"додавання інших точок до кривої."

#: ../../reference_manual/tools/path.rst:27
msgid ""
"Pressing the :kbd:`Del` key will remove the currently selected control point "
"from the curve. Double-click the |mouseleft| on any point of the curve or "
"press the :kbd:`Enter` key to finish drawing, or press the :kbd:`Esc` key to "
"cancel the entire curve. You can use the :kbd:`Ctrl` key while keeping the |"
"mouseleft| pressed to move the entire curve to a different position."
msgstr ""
"Натисканням клавіші :kbd:`Del` можна вилучити поточну позначену контрольну "
"точку з кривої. Двічі клацніть |mouseleft| у будь-якій точці кривої або "
"натисніть клавішу press :kbd:`Enter`, щоб завершити малювання кривої. Також "
"можна натиснути клавішу :kbd:`Esc`, щоб скасувати малювання усієї кривої. "
"Щоб пересунути усю криву у іншу позицію, натисніть і утримуйте клавішу :kbd:"
"`Ctrl` і перетягуйте криву, затиснувши |mouseleft|."

#: ../../reference_manual/tools/path.rst:29
msgid ""
"While drawing the :kbd:`Ctrl` key while dragging will push the handles both "
"ways. The :kbd:`Alt` key will create a sharp corner, and the :kbd:`Shift` "
"key will allow you to make a handle while at the end of the curve. |"
"mouseright| will undo the last added point."
msgstr ""
"Під час малювання ви можете скористатися натисканням клавіші :kbd:`Ctrl` під "
"час перетягування елементів керування для симетричного пересування елементів "
"керування. Натисканням клавіші :kbd:`Alt` під час клацання можна створити "
"гострий кут, а утримуванням натиснутою клавіші :kbd:`Shift` можна уможливити "
"створення елемента керування у кінці кривої. Клацання |mouseright| скасовує "
"додавання останньої доданої точки."

#: ../../reference_manual/tools/path.rst:32
msgid "Tool Options"
msgstr "Параметри інструмента"

#: ../../reference_manual/tools/path.rst:36
msgid "Autosmooth Curve"
msgstr "Автозгладжування кривої"

#: ../../reference_manual/tools/path.rst:37
msgid ""
"Toggling this will have nodes initialize with smooth curves instead of "
"angles. Untoggle this if you want to create sharp angles for a node. This "
"will not affect curve sharpness from dragging after clicking."
msgstr ""
"Позначення цього пункту призведе до того, що вузли контуру створюватимуться "
"як точки гладкої кривої, а не кутові точки. Зніміть позначення з цього "
"пункту, якщо хочете створювати гострі кути у вузлах контуру. Позначення чи "
"непозначення цього пункту не впливатиме на різкість сегмента кривої, який "
"утворюватиметься у результаті перетягування вказівника після натискання "
"лівої кнопки миші."

#: ../../reference_manual/tools/path.rst:39
msgid "Angle Snapping Delta"
msgstr "Крок кута прилипання"

#: ../../reference_manual/tools/path.rst:40
msgid "The angle to snap to."
msgstr "Кут для прилипання."

#: ../../reference_manual/tools/path.rst:42
msgid ""
"Angle snap will make it easier to have the next line be at a specific angle "
"of the current. The angle is determined by the :guilabel:`Angle Snapping "
"Delta`."
msgstr ""
"Прилипання за кутом спрощує створення кривих, у яких кожен наступний сегмент "
"буде розташовано під певним кутом до поточного. Кут визначається параметром :"
"guilabel:`Крок кута прилипання`."
