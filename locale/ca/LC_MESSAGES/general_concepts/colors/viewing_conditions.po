# Translation of docs_krita_org_general_concepts___colors___viewing_conditions.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-06 10:37+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/Krita_example_metamerism.png"
msgstr ".. image:: images/color_category/Krita_example_metamerism.png"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mix_up_ex1_01.svg"
msgstr ".. image:: images/color_category/White_point_mix_up_ex1_01.svg"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mixup_ex1_02.png"
msgstr ".. image:: images/color_category/White_point_mixup_ex1_02.png"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mix_up_ex1_03.svg"
msgstr ".. image:: images/color_category/White_point_mix_up_ex1_03.svg"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/Krita_metamerism_presentation.svg"
msgstr ".. image:: images/color_category/Krita_metamerism_presentation.svg"

#: ../../general_concepts/colors/viewing_conditions.rst:1
msgid "What are viewing conditions."
msgstr "Quines són les condicions de la visualització."

#: ../../general_concepts/colors/viewing_conditions.rst:10
#: ../../general_concepts/colors/viewing_conditions.rst:15
msgid "Viewing Conditions"
msgstr "Condicions de la visualització"

#: ../../general_concepts/colors/viewing_conditions.rst:10
msgid "Metamerism"
msgstr "Metamerisme"

#: ../../general_concepts/colors/viewing_conditions.rst:10
msgid "Color"
msgstr "Color"

#: ../../general_concepts/colors/viewing_conditions.rst:17
msgid ""
"We mentioned viewing conditions before, but what does this have to do with "
"'white points'?"
msgstr ""
"Abans hem esmentat les condicions de la visualització, però què té això a "
"veure amb els «punts blancs»?"

#: ../../general_concepts/colors/viewing_conditions.rst:19
msgid ""
"A lot actually, rather, white points describe a type of viewing condition."
msgstr ""
"Molt en realitat, més aviat, els punts blancs descriuen un tipus de condició "
"de la visualització."

#: ../../general_concepts/colors/viewing_conditions.rst:21
msgid ""
"So, usually what we mean by viewing conditions is the lighting and "
"decoration of the room that you are viewing the image in. Our eyes try to "
"make sense of both the colors that you are looking at actively (the colors "
"of the image) and the colors you aren't looking at actively (the colors of "
"the room), which means that both sets of colors affect how the image looks."
msgstr ""
"En general, el que entenem per condicions de la visualització és la "
"il·luminació i la decoració de l'habitació en la qual esteu veient la "
"imatge. Els nostres ulls intenten comprendre tant els colors que estan "
"veient activament (els colors de la imatge) com els colors que no estan "
"mirant activament (els colors de l'habitació), el qual vol dir que ambdós "
"conjunts de colors afecten a l'aparença de la imatge."

#: ../../general_concepts/colors/viewing_conditions.rst:27
msgid ".. image:: images/color_category/Meisje_met_de_parel_viewing.png"
msgstr ".. image:: images/color_category/Meisje_met_de_parel_viewing.png"

#: ../../general_concepts/colors/viewing_conditions.rst:27
msgid ""
"**Left**: Let's ruin Vermeer by putting a bright purple background that asks "
"for more attention than the famous painting itself. **Center**: a much more "
"neutral backdrop that an interior decorator would hate but brings out the "
"colors. **Right**: The approximate color that this painting is displayed "
"against in real life in the Maurits House, at the least, last time I was "
"there. Original image from wikipedia commons."
msgstr ""
"**Esquerra:** Arruïnem el Vermeer posant un fons porpra brillant que demana "
"més atenció que el famós quadre en si. **Centre:** Amb un teló de fons molt "
"més neutral que un decorador d'interiors odiaria, però que ressalta els "
"colors. **Dreta:** Amb el color aproximat contra el qual es mostra aquesta "
"pintura a la vida real a la casa Maurits, almenys, l'última vegada que hi "
"vaig estar. Imatge original de la «wikipedia commons»."

#: ../../general_concepts/colors/viewing_conditions.rst:29
msgid ""
"This is for example, the reason why museum exhibitioners can get really "
"angry at the interior decorators when the walls of the museum are painted "
"bright red or blue, because this will drastically change the way how the "
"painting's colors look. (Which, if we are talking about a painter known for "
"their colors like Vermeer, could result in a really bad experience)."
msgstr ""
"Aquesta és, per exemple, la raó per la qual els expositors del museu es "
"poden enutjar molt amb els decoradors d'interiors quan les parets del museu "
"estan pintades de vermell brillant o blau, perquè això canviarà dràsticament "
"la forma en què es veuran els colors de la pintura. (El qual, si estem "
"parlant d'un pintor conegut pels seus colors com en Vermeer, podria resultar "
"en una experiència realment dolenta)."

# skip-rule: t-acc_obe
#: ../../general_concepts/colors/viewing_conditions.rst:37
msgid ""
"Lighting is the other component of the viewing condition which can have "
"dramatic effects. Lighting in particular affects the way how all colors "
"look. For example, if you were to paint an image of sunflowers and poppies, "
"print that out, and shine a bright yellow light on it, the sunflowers would "
"become indistinguishable from the white background, and the poppies would "
"look orange. This is called `metamerism <https://en.wikipedia.org/wiki/"
"Metamerism_%28color%29>`_, and it's generally something you want to avoid in "
"your color management pipeline."
msgstr ""
"La il·luminació és l'altre component de la condició de la visualització que "
"pot tenir efectes dramàtics. La il·luminació en particular afectarà a la "
"forma en què es veuran tots els colors. Per exemple, si pinteu una imatge de "
"girasols i roselles, imprimiu-la i feu brillar una llum groga brillant sobre "
"seu, els gira-sols es tornaran indistingibles del fons blanc i les roselles "
"es veuran de color taronja. Això s'anomena `metamerisme <https://ca."
"wikipedia.org/wiki/Metamerisme>`_ i, en general, és una cosa que cal evitar "
"en la vostra canalització de la gestió del color."

#: ../../general_concepts/colors/viewing_conditions.rst:39
msgid ""
"An example where metamerism could become a problem is when you start "
"matching colors from different sources together."
msgstr ""
"Un exemple en el qual el metamerisme podria esdevenir en un problema és quan "
"comenceu a combinar colors des de diferents fonts."

#: ../../general_concepts/colors/viewing_conditions.rst:46
msgid ""
"For example, if you are designing a print for a red t-shirt that's not "
"bright red, but not super grayish red either. And you want to make sure the "
"colors of the print match the color of the t-shirt, so you make a dummy "
"background layer that is approximately that red, as correctly as you can "
"observe it, and paint on layers above that dummy layer. When you are done, "
"you hide this dummy layer and sent the image with a transparent background "
"to the press."
msgstr ""
"Per exemple, si esteu dissenyant una impressió per a una samarreta vermella "
"que no és de color vermell brillant, però tampoc és de color súper vermell "
"grisenc. I voleu assegurar-vos que els colors de la impressió coincideixin "
"amb el color de la samarreta, de manera que creeu una capa de fons fictícia "
"que serà aproximadament d'aquest vermell, tan correctament com pugueu "
"observar-lo, i pinteu sobre les capes que es trobin per sobre d'aquesta capa "
"fictícia. En acabar, oculteu aquesta capa fictícia i envieu la imatge amb un "
"fons transparent a la impremta."

#: ../../general_concepts/colors/viewing_conditions.rst:54
msgid ""
"But when you get the t-shirt from the printer, you notice that all your "
"colors look off, mismatched, and maybe too yellowish (and when did that T-"
"Shirt become purple?)."
msgstr ""
"Però quan obteniu la samarreta de la impremta, observeu que tots els colors "
"es veuen apagats, desiguals i potser massa groguencs (i quan s'ha tornat "
"porpra aquesta samarreta?)."

#: ../../general_concepts/colors/viewing_conditions.rst:56
msgid "This is where white points come in."
msgstr "Aquí és on entren els punts blancs."

#: ../../general_concepts/colors/viewing_conditions.rst:58
msgid ""
"You probably observed the t-shirt in a white room where there were "
"incandescent lamps shining, because as a true artist, you started your work "
"in the middle of the night, as that is when the best art is made. However, "
"incandescent lamps have a black body temperature of roughly 2300-2800K, "
"which makes them give a yellowish light, officially called White Point A."
msgstr ""
"Probablement heu observat la samarreta en una habitació blanca on brillaven "
"làmpades incandescents, perquè com un veritable artista, heu començat a "
"treballar enmig de la nit, ja que és quan es fa el millor art. No obstant "
"això, les làmpades incandescents tenen una temperatura del cos negre "
"d'aproximadament 2.300 a 2.800 K, la qual les fa donar una llum groguenca, "
"oficialment anomenada Punt blanc A."

#: ../../general_concepts/colors/viewing_conditions.rst:61
msgid ""
"Your computer screen on the other hand, has a black body temperature of "
"6500K, also known as D65. Which is a far more blueish color of light than "
"the lamps you are hanging."
msgstr ""
"D'altra banda, la pantalla del vostre ordinador té una temperatura del cos "
"negre de 6.500 K, també coneguda com a D65. La qual crea un color de la llum "
"molt més blau que les làmpades que heu penjat."

#: ../../general_concepts/colors/viewing_conditions.rst:63
msgid ""
"What's worse, Printers print on the basis of using a white point of D50, the "
"color of white paper under direct sunlight."
msgstr ""
"El que és pitjor, les impressores imprimeixen sobre la base que s'empra un "
"punt blanc D50, el color del paper blanc sota la llum solar directa."

#: ../../general_concepts/colors/viewing_conditions.rst:70
msgid ""
"So, by eye-balling your t-shirt's color during the evening, you took its red "
"color as transformed by the yellowish light. Had you made your observation "
"in diffuse sunlight of an overcast (which is also roughly D65), or made it "
"in direct sunlight light and painted your picture with a profile set to D50, "
"the color would have been much closer, and thus your design would not be as "
"yellowish."
msgstr ""
"Llavors, tenint en compte el color de la vostra samarreta durant la nit, heu "
"pres el seu color vermell transformat per la llum groguenca. Si l'haguéssiu "
"observat amb la llum solar d'un dia fosc (la qual també és aproximadament "
"D65), o bé haguéssiu creat la imatge amb llum solar directa i pintat amb un "
"perfil establert a D50, el color hauria estat molt més proper, de manera que "
"el vostre disseny no seria tan groguenc."

#: ../../general_concepts/colors/viewing_conditions.rst:77
msgid ".. image:: images/color_category/White_point_mixup_ex1_03.png"
msgstr ".. image:: images/color_category/White_point_mixup_ex1_03.png"

#: ../../general_concepts/colors/viewing_conditions.rst:77
msgid ""
"Applying a white balance filter will sort of match the colors to the tone as "
"in the middle, but you would have had a much better design had you designed "
"against the actual color to begin with."
msgstr ""
"L'aplicació d'un filtre amb balanç de blancs farà que els colors "
"coincideixin amb el to mitjà, però es pot obtenir un disseny molt millor si "
"en començar haguéssiu dissenyat contra el color real."

#: ../../general_concepts/colors/viewing_conditions.rst:79
msgid ""
"Now, you could technically quickly fix this by using a white balancing "
"filter, like the ones in G'MIC, but because this error is caught at the end "
"of the production process, you basically limited your use of possible colors "
"when you were designing, which is a pity."
msgstr ""
"Ara, des del punt de vista tècnic, podríeu solucionar-ho ràpidament "
"utilitzant un filtre amb balanç de blancs, com els de G'MIC, però com aquest "
"error s'ha detectat al final del procés de producció, per desgràcia, es "
"limitarà a l'ús de possibles colors mentre esteu dissenyant."

#: ../../general_concepts/colors/viewing_conditions.rst:81
msgid ""
"Another example where metamerism messes things up is with screen projections."
msgstr ""
"Un altre exemple on el metamerisme confondrà les coses és amb les "
"projeccions de la pantalla."

#: ../../general_concepts/colors/viewing_conditions.rst:83
msgid ""
"We have a presentation where we mark one type of item with red, another with "
"yellow and yet another with purple. On a computer the differences between "
"the colors are very obvious."
msgstr ""
"Tenim una presentació on hem marcat un tipus d'element amb vermell, un altre "
"amb groc i un altre amb porpra. En un ordinador les diferències entre els "
"colors són molt òbvies."

#: ../../general_concepts/colors/viewing_conditions.rst:89
msgid ""
"However, when we start projecting, the lights of the room aren't dimmed, "
"which means that the tone scale of the colors becomes crunched, and yellow "
"becomes near indistinguishable from white. Furthermore, because the light in "
"the room is slightly yellowish, the purple is transformed into red, making "
"it indistinguishable from the red. Meaning that the graphic is difficult to "
"read."
msgstr ""
"No obstant això, quan hem començat a projectar, les llums de l'habitació no "
"s'atenuen, el qual vol dir que l'escala de tons dels colors es distorsionarà "
"i que el groc es tornarà gairebé indistingible del blanc. A més, a causa que "
"la llum a l'habitació és lleugerament groguenca, el porpra es transformarà "
"en vermell, pel qual és indistingible del vermell. El qual voldrà dir que el "
"gràfic és difícil de llegir."

#: ../../general_concepts/colors/viewing_conditions.rst:91
msgid ""
"In both cases, you can use Krita's color management a little to help you, "
"but mostly, you just need to be ''aware'' of it, as Krita can hardly fix "
"that you are looking at colors at night, or the fact that the presentation "
"hall owner refuses to turn off the lights."
msgstr ""
"En ambdós casos, podreu utilitzar una mica la gestió del color del Krita per "
"ajudar, però sobretot, només necessiteu ser-ne «conscient», ja que el Krita "
"no pot arreglar que estigueu veient els colors durant la nit, o el fet que "
"el propietari de la sala de presentació es negui a apagar els llums."

#: ../../general_concepts/colors/viewing_conditions.rst:93
msgid ""
"That said, unless you have a display profile that uses LUTs, such as an OCIO "
"LUT or a cLUT icc profile, white point won't matter much when choosing a "
"working space, due to weirdness in the icc v4 workflow which always converts "
"matrix profiles with relative colorimetric, meaning the white points are "
"matched up."
msgstr ""
"Dit això, llevat que tingueu un perfil de visualització que utilitzi les "
"LUT, com la LUT a OCIO o un perfil ICC amb les cLUT, el punt blanc no "
"importarà molt en triar un espai de treball, a causa de la raresa en el flux "
"de treball amb ICC v4, el qual sempre convertirà els perfils de matriu amb "
"colorimetria relativa, el qual vol dir que els punts blancs coincideixen."
