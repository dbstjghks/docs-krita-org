msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../reference_manual/dockers/color_sliders.rst:1
msgid "Overview of the color sliders docker."
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:11
#: ../../reference_manual/dockers/color_sliders.rst:16
msgid "Color Sliders"
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Color"
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Color Selector"
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Hue"
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Saturation"
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Value"
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Brightness"
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Lightness"
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Intensity"
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Luma"
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Luminosity"
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:20
msgid ""
"This docker has been removed in 4.1. It will return in some form in the "
"future."
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:22
msgid "A small docker with Hue, Saturation and Lightness bars."
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:25
msgid ".. image:: images/dockers/Color-slider-docker.png"
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:26
msgid ""
"You can configure this docker via :menuselection:`Settings --> Configure "
"Krita --> Color Selector Settings --> Color Sliders`."
msgstr ""

#: ../../reference_manual/dockers/color_sliders.rst:28
msgid ""
"There, you can select which sliders you would like to see added, allowing "
"you to even choose multiple lightness sliders together."
msgstr ""
