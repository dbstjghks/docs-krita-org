# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-30 16:19+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Mix with background color."
msgstr "Met achtergrondkleur mengen."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:1
msgid "The Spray Brush Engine manual page."
msgstr "De handleidingpagina Spuitbuspenseel-engine."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:16
msgid "Spray Brush Engine"
msgstr "Spuitbuspenseel-engine"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Penseel-engine"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:11
msgid "Airbrush"
msgstr "Airbrush"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:19
msgid ".. image:: images/icons/spraybrush.svg"
msgstr ".. image:: images/icons/spraybrush.svg"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:20
msgid "A brush that can spray particles around in its brush area."
msgstr "Een penseel die deeltjes rondspuit in zijn penseelgebied."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:23
msgid "Options"
msgstr "Opties"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:25
msgid ":ref:`option_spray_area`"
msgstr ":ref:`option_spray_area`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:26
msgid ":ref:`option_spray_shape`"
msgstr ":ref:`option_spray_shape`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:27
msgid ":ref:`option_brush_tip` (Used as particle if spray shape is not active)"
msgstr ""
":ref:`option_brush_tip` (gebruikt als deeltje als spuitvorm niet actief is)"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:28
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:29
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:30
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:31
msgid ":ref:`option_shape_dyna`"
msgstr ":ref:`option_shape_dyna`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:32
msgid ":ref:`option_color_spray`"
msgstr ":ref:`option_color_spray`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:33
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:34
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:39
msgid "Spray Area"
msgstr "Spuitgebied"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:41
msgid "The area in which the particles are sprayed."
msgstr "Het gebied waarin de deeltjes gespoten worden."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:43
msgid "Diameter"
msgstr "Diameter"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:44
msgid "The size of the area."
msgstr "De grootte van het gebied."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:45
msgid "Aspect Ratio"
msgstr "Beeldverhouding"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:46
msgid "It's aspect ratio: 1.0 is fully circular."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:47
msgid "Angle"
msgstr "Hoek"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:48
msgid ""
"The angle of the spray size: works nice with aspect ratios other than 1.0."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:49
msgid "Scale"
msgstr "Schaal"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:50
msgid "Scales the diameter up."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:52
msgid "Spacing"
msgstr "Spatiëring"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:52
msgid "Increases the spacing of the diameter's spray."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:55
msgid "Particles"
msgstr "Deeltjes"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:57
msgid "Count"
msgstr "Aantal"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:58
msgid "Use a specified amount of particles."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:59
msgid "Density"
msgstr "Dichtheid"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:60
msgid "Use a % amount of particles."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:61
msgid "Jitter Movement"
msgstr "Beweging met trilling"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:62
msgid "Jitters the spray area around for extra randomness."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:64
msgid "Gaussian Distribution"
msgstr "Normale verdeling"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:64
msgid ""
"Focuses the particles to paint in the center instead of evenly random over "
"the spray area."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:69
msgid "Spray Shape"
msgstr "Spuitvorm"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:71
msgid ""
"If activated, this will generate a special particle. If not, the brush-tip "
"will be the particle."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:74
msgid "Can be..."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:76
msgid "Ellipse"
msgstr "Ellips"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:77
msgid "Rectangle"
msgstr "Rechthoek"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:78
msgid "Anti-aliased Pixel"
msgstr "Pixel met anti-aliasing"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:79
msgid "Pixel"
msgstr "Pixel"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:80
msgid "Shape"
msgstr "Vorm"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:80
msgid "Image"
msgstr "Afbeelding"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:82
msgid "Width & Height"
msgstr "Breedte & hoogte"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:83
msgid "Decides the width and height of the particle."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:84
msgid "Proportional"
msgstr "Proportioneel"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:85
msgid "Locks Width & Height to be the same."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:87
msgid "Texture"
msgstr "Textuur"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:87
msgid "Allows you to pick an image for the :guilabel:`Image shape`."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:92
msgid "Shape Dynamics"
msgstr "Vorm-dynamica"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:94
msgid "Random Size"
msgstr "Willekeurige grootte"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:95
msgid ""
"Randomizes the particle size between 1x1 px and the given size of the "
"particle in brush-tip or spray shape."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:96
msgid "Fixed Rotation"
msgstr "Vaste rotatie"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:97
msgid "Gives a fixed rotation to the particle to work from."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:98
msgid "Randomized Rotation"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:99
msgid "Randomizes the rotation."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:100
msgid "Follow Cursor Weight"
msgstr "Cursorgewicht volgen"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:101
msgid ""
"How much the pressure affects the rotation of the particles. At 1.0 and high "
"pressure it'll seem as if the particles are exploding from the middle."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:103
msgid "Angle Weight"
msgstr "Hoekgewicht:"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:103
msgid "How much the spray area angle affects the particle angle."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:108
msgid "Color Options"
msgstr "Kleuropties"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:110
msgid "Random HSV"
msgstr "Willekeurige HSV"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:111
msgid ""
"Randomize the HSV with the strength of the sliders. The higher, the more the "
"color will deviate from the foreground color, with the direction indicating "
"clock or counter clockwise."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:112
msgid "Random Opacity"
msgstr "Willekeurige dekking"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:113
msgid "Randomizes the opacity."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:114
msgid "Color Per Particle"
msgstr "Kleur per deeltje"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:115
msgid "Has the color options be per particle instead of area."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:116
msgid "Sample Input Layer."
msgstr "Voorbeeld invoerlaag."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:117
msgid ""
"Will use the underlying layer as reference for the colors instead of the "
"foreground color."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:118
msgid "Fill Background"
msgstr "Achtergrond vullen"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:119
msgid "Fills the area before drawing the particles with the background color."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:121
msgid ""
"Gives the particle a random color between foreground/input/random HSV and "
"the background color."
msgstr ""
