# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-30 16:09+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:1
msgid "The Pixel Brush Engine manual page."
msgstr "De handleidingpagina Pixelpenseel-engine."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:16
msgid "Pixel Brush Engine"
msgstr "Pixelpenseel-engine"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Penseel-engine"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:19
msgid ".. image:: images/icons/pixelbrush.svg"
msgstr ".. image:: images/icons/pixelbrush.svg"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:20
msgid ""
"Brushes are ordered alphabetically. The brush that is selected by default "
"when you start with Krita is the :guilabel:`Pixel Brush`. The pixel brush is "
"the traditional mainstay of digital art. This brush paints impressions of "
"the brush tip along your stroke with a greater or smaller density."
msgstr ""
"Penselen zijn alfabetisch geordend. Het penseel dat standaard wordt "
"geselecteerd wanneer u Krita start is het :guilabel:`Pixelpenseel`. Het "
"pixelpenseel is de traditionele steunpilaar van digitale kunst. Dit penseel "
"schildert indrukkingen van de penseeltop langs uw streek met een grotere of "
"kleinere dichtheid."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:24
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Popup.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Popup.png"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:25
msgid "Let's first review these mechanics:"
msgstr "Laten we eerst deze mechanismen langlopen:"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:27
msgid ""
"Select a brush tip. This can be a generated brush tip (round, square, star-"
"shaped), a predefined bitmap brush tip, a custom brush tip or a text."
msgstr ""
"Selecteer een penseeltop. Dit kan een gegenereerde penseeltop zijn (rond, "
"vierkant, stervormig), een voorgedefinieerde bitmap penseeltop, een "
"aangepaste penseeltop of een tekst."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:28
msgid ""
"Select the spacing: this determines how many impressions of the tip will be "
"made along your stroke"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:29
msgid ""
"Select the effects: the pressure of your stylus, your speed of painting or "
"other inputs can change the size, the color, the opacity or other aspects of "
"the currently painted brush tip instance -- some applications call that a "
"\"dab\"."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:30
msgid ""
"Depending on the brush mode, the previously painted brush tip instance is "
"mixed with the current one, causing a darker, more painterly stroke, or the "
"complete stroke is computed and put on your layer. You will see the stroke "
"grow while painting in both cases, of course!"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:32
msgid ""
"Since 4.0, the Pixel Brush Engine has Multithreaded brush-tips, with the "
"default brush being the fastest mask."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:34
msgid "Available Options:"
msgstr "Beschikbare opties:"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:36
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:37
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:38
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:39
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:40
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:41
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:42
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:43
msgid ":ref:`option_softness`"
msgstr ":ref:`option_softness`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:44
msgid ":ref:`option_sharpness`"
msgstr ":ref:`option_sharpness`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:45
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:46
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:47
msgid ":ref:`option_source`"
msgstr ":ref:`option_source`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:48
msgid ":ref:`option_mix`"
msgstr ":ref:`option_mix`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:49
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:50
msgid ":ref:`option_texture`"
msgstr ":ref:`option_texture`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:51
msgid ":ref:`option_masked_brush`"
msgstr ":ref:`option_masked_brush`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:54
msgid "Specific Parameters to the Pixel Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:57
msgid "Darken"
msgstr "Donkerder maken"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:59
msgid "Allows you to Darken the source color with Sensors."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:62
msgid ".. image:: images/brushes/Krita_2_9_brushengine_darken_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_darken_01.png"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:63
msgid ""
"The color will always become black in the end, and will work with Plain "
"Color, Gradient and Uniform random as source."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:66
msgid "Hue, Saturation, Value"
msgstr "Tint, verzadiging, intensiteit"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:68
msgid ""
"These parameters allow you to do an HSV adjustment filter on the :ref:"
"`option_source` and control it with Sensors."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:71
msgid ".. image:: images/brushes/Krita_2_9_brushengine_HSV_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_HSV_01.png"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:72
msgid "Works with Plain Color, Gradient and Uniform random as source."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:75
msgid "Uses"
msgstr "Gebruiken"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:78
msgid ".. image:: images/brushes/Krita_2_9_brushengine_HSV_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_HSV_02.png"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:79
msgid ""
"Having all three parameters on Fuzzy will help with rich color texture. In "
"combination with :ref:`option_mix`, you can have even finer control."
msgstr ""
