# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-07 10:31+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:62
msgid ""
".. image:: images/icons/smart_patch_tool.svg\n"
"   :alt: toolsmartpatch"
msgstr ""
".. image:: images/icons/smart_patch_tool.svg\n"
"   :alt: toolsmartpatch"

#: ../../reference_manual/tools/smart_patch.rst:1
msgid "Krita's smart patch tool reference."
msgstr "Verwijzing naar hulpmiddel Slim verbeteren van Krita."

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Tools"
msgstr "Hulpmiddelen"

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Smart Patch"
msgstr "Slim verbeteren"

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Automatic Healing"
msgstr ""

#: ../../reference_manual/tools/smart_patch.rst:16
msgid "Smart Patch Tool"
msgstr "Hulpmiddel Slim verbeteren"

#: ../../reference_manual/tools/smart_patch.rst:18
msgid "|toolsmartpatch|"
msgstr "|toolsmartpatch|"

#: ../../reference_manual/tools/smart_patch.rst:20
msgid ""
"The smart patch tool allows you to seamlessly remove elements from the "
"image. It does this by letting you draw the area which has the element you "
"wish to remove, and then it will attempt to use patterns already existing in "
"the image to fill the blank."
msgstr ""

#: ../../reference_manual/tools/smart_patch.rst:22
msgid "You can see it as a smarter version of the clone brush."
msgstr ""

#: ../../reference_manual/tools/smart_patch.rst:25
msgid ".. image:: images/tools/Smart-patch.gif"
msgstr ""

#: ../../reference_manual/tools/smart_patch.rst:26
msgid "The smart patch tool has the following tool options:"
msgstr ""

#: ../../reference_manual/tools/smart_patch.rst:29
msgid "Accuracy"
msgstr "Nauwkeurigheid"

#: ../../reference_manual/tools/smart_patch.rst:31
msgid ""
"Accuracy indicates how many samples, and thus how often the algorithm is "
"run. A low accuracy will do few samples, but will also run the algorithm "
"fewer times, making it faster. Higher accuracy will do many samples, making "
"the algorithm run more often and give more precise results, but because it "
"has to do more work, it is slower."
msgstr ""

#: ../../reference_manual/tools/smart_patch.rst:34
msgid "Patch size"
msgstr ""

#: ../../reference_manual/tools/smart_patch.rst:36
msgid ""
"Patch size determines how big the size of the pattern to choose is. This "
"will be best explained with some testing, but if the surrounding image has "
"mostly small elements, like branches, a small patch size will give better "
"results, while a big patch size will be better for images with big elements, "
"so they get reused as a whole."
msgstr ""
