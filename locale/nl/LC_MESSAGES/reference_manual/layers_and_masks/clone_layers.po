# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-06 11:32+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:1
msgid "How to use clone layers."
msgstr "Hoe kloonlagen gebruiken."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Layers"
msgstr "Lagen"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Linked Clone"
msgstr "Gekoppelde kloon"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Clone Layer"
msgstr "Kloonlaag"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:20
msgid "Clone Layers"
msgstr "Kloonlagen"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:22
msgid ""
"A clone layer is a layer that keeps an up-to-date copy of another layer. You "
"cannot draw or paint on it directly, but it can be used to create effects by "
"applying different types of layers and masks (e.g. filter layers or masks)."
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:25
msgid "Example uses of Clone Layers."
msgstr "Voorbeeld van gebruik van kloonlagen."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:27
msgid ""
"For example, if you were painting a picture of some magic person and wanted "
"to create a glow around them that was updated as you updated your character, "
"you could:"
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:29
msgid "Have a Paint Layer where you draw your character"
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:30
msgid ""
"Use the Clone Layer feature to create a clone of the layer that you drew "
"your character on"
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:31
msgid ""
"Apply an HSV filter mask to the clone layer to make the shapes on it white "
"(or blue, or green etc.)"
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:32
msgid "Apply a blur filter mask to the clone layer so it looks like a \"glow\""
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:34
msgid ""
"As you keep painting and adding details, erasing on the first layer, Krita "
"will automatically update the clone layer, making your \"glow\" apply to "
"every change you make."
msgstr ""
