# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# pan93412 <pan93412@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-04 21:09+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@linux.org.tw>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 19.03.80\n"

#: ../../user_manual.rst:5
msgid "User Manual"
msgstr "使用者手冊"

#: ../../user_manual.rst:7
msgid ""
"Discover Krita’s features through an online manual. Guides to help you "
"transition from other applications."
msgstr ""

#: ../../user_manual.rst:9
msgid "Contents:"
msgstr "內容："
