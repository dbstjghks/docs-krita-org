# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-30 20:34+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/file_formats/file_gif.rst:1
msgid "The GIF file format in Krita."
msgstr "Filformatet GIF i Krita."

#: ../../general_concepts/file_formats/file_gif.rst:10
msgid "GIF"
msgstr "GIF"

#: ../../general_concepts/file_formats/file_gif.rst:10
msgid "*.gif"
msgstr "*.gif"

#: ../../general_concepts/file_formats/file_gif.rst:15
msgid "\\*.gif"
msgstr "\\*.gif"

#: ../../general_concepts/file_formats/file_gif.rst:17
msgid ""
"``.gif`` is a file format mostly known for the fact that it can save "
"animations. It's a fairly old format, and it does its compression by :ref:"
"`indexing <bit_depth>` the colors to a maximum of 256 colors per frame. "
"Because we can technically design an image for 256 colors and are always "
"able save over an edited GIF without any kind of extra degradation, this is "
"a :ref:`lossless <lossless_compression>` compression technique."
msgstr ""
"Filformatet ``.gif`` är mest känt för att det kan spara animeringar. Det är "
"ett ganska gammalt format, och komprimering görs med att :ref:`indexera "
"<bit_depth>` färgerna till maximalt 256 färger per bildruta. Eftersom vi "
"tekniskt sett kan skapa en bild med 256 färger, och alltid kan skriva över "
"en redigerad GIF utan någon extra degradering, är det en :ref:`förlustfri "
"<lossless_compression>` komprimeringsteknik."

#: ../../general_concepts/file_formats/file_gif.rst:19
msgid ""
"This means that it can handle most grayscale images just fine and without "
"losing any visible quality. But for color images that don't animate it might "
"be better to use :ref:`file_jpg` or :ref:`file_png`."
msgstr ""
"Det betyder att den kan hantera de flesta gråskalebilder utmärkt, och utan "
"att förlora någon synlig kvalitet. Men för färgbilder som inte är animerade "
"kan det vara bättre att använda :ref:`file_jpg` eller :ref:`file_png`."
