.. meta::
   :description:
        The edit menu in Krita.

.. metadata-placeholder

   :authors: - Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
             - Scott Petrovic
   :license: GNU free documentation license 1.3 or later.

.. index:: Edit, Undo, Redo, Cut, Copy, Paste
.. _edit_menu:

=========
Edit Menu
=========

.. glossary::

    Undo
        Undoes the last action. Shortcut: :kbd:`Ctrl + Z`

    Redo
        Redoes the last undone action. Shortcut: :kbd:`Ctrl + Shift+ Z`

    Cut
        Cuts the selection or layer. Shortcut: :kbd:`Ctrl + X`

    Copy
        Copies the selection or layer. Shortcut: :kbd:`Ctrl + C`

    Cut (Sharp)
        This prevents semi-transparent areas from appearing on your cut pixels, making them either fully opaque or fully transparent.

    Copy (Sharp)
        Same as :term:`Cut (Sharp)` but then copying instead.

    Copy Merged
        Copies the selection over all layers. Shortcut: :kbd:`Ctrl + Shift + C`

    Paste
        Pastes the copied buffer into the image as a new layer. Shortcut: :kbd:`Ctrl + V`

    Paste at Cursor
        Same as :term:`paste`, but aligns the image to the cursor. Shortcut: :kbd:`Ctrl + Alt + V`

    Paste into New Image
        Pastes the copied buffer into a new image. Shortcut: :kbd:`Ctrl + Shift + N`

    Clear
        Clear the current layer. Shortcut: :kbd:`Del`

    Fill with Foreground Color
        Fills the layer or selection with the foreground color. Shortcut: :kbd:`Shift + Backspace`

    Fill with Background Color
        Fills the layer or selection with the background color. Shortcut: :kbd:`Backspace`

    Fill with Pattern
        Fills the layer or selection with the active pattern.
        
    Stroke Selected Shapes
        Strokes the selected vector shape with the selected brush, will create a new layer.

    Stroke Selection
        Strokes the active selection using the menu.
